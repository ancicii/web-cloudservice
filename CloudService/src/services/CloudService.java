package services;

import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.HashMap;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import beans.Disk;
import beans.Diskovi;
import beans.Kategorije;
import beans.Korisnici;
import beans.Korisnik;
import beans.Organizacija;
import beans.Organizacije;
import beans.UserToLogin;
import beans.VirtualnaMasina;
import beans.VirtualneMasine;
import json.DiskJSON;
import json.KorisnikJSON;
import json.OrganizacijaJSON;
import json.VirtualnaMasinaJSON;

@DeclareRoles({ "SUPER_ADMIN", "ADMIN", "KORISNIK" })
@Path("/cloudy")
public class CloudService {

	@Context
	HttpServletRequest request;
	@Context
	SecurityContext securityContext;
	@Context
	ServletContext ctx;
	Korisnici korisnici;
	Kategorije kategorije;
	Diskovi diskovi;
	VirtualneMasine virtualneMasine;
	Organizacije organizacije;

	Gson gson=  Converters.registerOffsetDateTime(new GsonBuilder().setPrettyPrinting()).create();

	void dodajOrganizacijeKorisnicima() {
		for (Organizacija o : organizacije.getOrganizacije().values()) {
			for (Korisnik k : o.getKorisnici()) {
				korisnici.getKorisnici().get(k.getEmail()).setOrganizacija(o);
			}
		}
	}

	public CloudService() {
		korisnici = new Korisnici();
		kategorije = new Kategorije();
		diskovi = new Diskovi();
		virtualneMasine = new VirtualneMasine(diskovi);
		organizacije = new Organizacije(diskovi, virtualneMasine);
		dodajOrganizacijeKorisnicima();
	}

	@POST
	@Path("/login")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response login(UserToLogin u) throws Exception {
		if (u.getEmail() != null && !u.getEmail().equals("")) {

			if (korisnici.getKorisnici().containsKey(u.getEmail())) {
				Korisnik korisnik = korisnici.getKorisnici().get(u.getEmail());
				if (korisnik.getLozinka().equals(u.getLozinka())) {
					if (korisnik.getUloga().toString().equals("KORISNIK")) {
						return Response.status(Response.Status.OK).entity("KORISNIK").build();
					} else if (korisnik.getUloga().toString().equals("ADMIN")) {
						return Response.status(Response.Status.OK).entity("ADMIN").build();
					} else {
						return Response.status(Response.Status.OK).entity("SUPER_ADMIN").build();
					}
				} else {
					return Response.status(Response.Status.BAD_REQUEST).entity("Pogresno korisnicko ime ili sifra")
							.build();
				}
			} else {
				return Response.status(Response.Status.BAD_REQUEST).entity("Pogresno korisnicko ime ili sifra").build();
			}
		}
		return Response.status(Response.Status.BAD_REQUEST).entity("Morate popuniti sva polja").build();

	}

	@GET
	@RolesAllowed({ "SUPER_ADMIN", "ADMIN", "KORISNIK" })
	@Path("/logout")
	@Produces(MediaType.APPLICATION_JSON)
	public Response logout() {
		request.getSession().invalidate();
		return Response.status(Response.Status.OK).entity("Uspesno odjavljen korisnik!").build();
	}

	// UPISIVANJE ORGANIZACIJE U FAJL
	public void upisiOrganizacije() {
		try {
			PrintWriter writer = new PrintWriter(
					"D:\\Ana\\Web Projekat\\web-cloudservice\\CloudService\\WebContent\\resources\\organizacije.txt",
					"UTF-8");
			HashMap<String, OrganizacijaJSON> organizacijeJSON = new HashMap<String, OrganizacijaJSON>();
			for (Organizacija novaO : organizacije.getOrganizacije().values()) {
				OrganizacijaJSON oJSON = new OrganizacijaJSON(novaO);
				organizacijeJSON.put(oJSON.getIme(), oJSON);
			}
			Type listType = new TypeToken<HashMap<String, OrganizacijaJSON>>() {
			}.getType();
			String upisi = gson.toJson(organizacijeJSON, listType);
			writer.println(upisi);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// UPISIVANJE KORISNIKA U FAJL
	public void upisiKorisnike() {
		try {
			PrintWriter writer = new PrintWriter(
					"D:\\Ana\\Web Projekat\\web-cloudservice\\CloudService\\WebContent\\resources\\korisnici.txt",
					"UTF-8");
			HashMap<String, KorisnikJSON> korisniciJSON = new HashMap<String, KorisnikJSON>();
			for (Korisnik noviKorisnik : korisnici.getKorisnici().values()) {
				KorisnikJSON korisnikJSON = new KorisnikJSON(noviKorisnik);
				korisniciJSON.put(korisnikJSON.getEmail(), korisnikJSON);
			}
			Type listType = new TypeToken<HashMap<String, KorisnikJSON>>() {
			}.getType();
			String upisi = gson.toJson(korisniciJSON, listType);
			writer.println(upisi);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// UPISIVANJE VIRTUALNIH MASINA U FAJL
	public void upisiVirtualneMasine() {
		try {
			PrintWriter writer = new PrintWriter(
					"D:\\Ana\\Web Projekat\\web-cloudservice\\CloudService\\WebContent\\resources\\virtualneMasine.txt",
					"UTF-8");
			HashMap<String, VirtualnaMasinaJSON> virtualneMasineJSON = new HashMap<String, VirtualnaMasinaJSON>();
			for (VirtualnaMasina novaVM : virtualneMasine.getVirtualneMasine().values()) {
				VirtualnaMasinaJSON virtualnaMasinaJSON = new VirtualnaMasinaJSON(novaVM);
				virtualneMasineJSON.put(virtualnaMasinaJSON.getIme(), virtualnaMasinaJSON);
			}
			Type listType = new TypeToken<HashMap<String, VirtualnaMasinaJSON>>() {}.getType();
			Gson gson1 =  Converters.registerOffsetDateTime(new GsonBuilder().setPrettyPrinting()
			        .serializeNulls()).create();
			String upisi = gson1.toJson(virtualneMasineJSON, listType);
			writer.println(upisi);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// UPISIVANJE DISKOVA U FAJL
		public void upisiDiskove() {
			try {
				PrintWriter writer = new PrintWriter(
						"D:\\Ana\\Web Projekat\\web-cloudservice\\CloudService\\WebContent\\resources\\diskovi.txt",
						"UTF-8");
				HashMap<String, DiskJSON> diskoviJSON = new HashMap<String, DiskJSON>();
				for (Disk noviDisk : diskovi.getDiskovi().values()) {
					DiskJSON diskJSON = new DiskJSON(noviDisk.getIme(), noviDisk.getTip().toString(), noviDisk.getKapacitet());
					diskoviJSON.put(diskJSON.getIme(), diskJSON);
				}
				Type listType = new TypeToken<HashMap<String, DiskJSON>>() {}.getType();
				String upisi = gson.toJson(diskoviJSON, listType);
				writer.println(upisi);
				writer.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

}
