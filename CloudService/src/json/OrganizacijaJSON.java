package json;

import java.util.ArrayList;

import beans.Disk;
import beans.Diskovi;
import beans.Korisnik;
import beans.Organizacija;
import beans.VirtualnaMasina;
import beans.VirtualneMasine;
import exceptions.NotUniqueException;

public class OrganizacijaJSON {
	
	private String ime;
	private String opis;
	private String logo;
	private ArrayList<KorisnikJSON> korisnici;
	private ArrayList<String> resursi;
	
	public ArrayList<KorisnikJSON> getKorisnici() {
		return korisnici;
	}
	public void setKorisnici(ArrayList<KorisnikJSON> korisnici) {
		this.korisnici = korisnici;
	}
	public ArrayList<String> getResursi() {
		return resursi;
	}
	public void setResursi(ArrayList<String> resursi) {
		this.resursi = resursi;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public OrganizacijaJSON(String ime, String opis, String logo, ArrayList<KorisnikJSON> korisnici,
			ArrayList<String> resursi) {
		super();
		this.ime = ime;
		this.opis = opis;
		this.logo = logo;
		this.korisnici = korisnici;
		this.resursi = resursi;
	}
	public OrganizacijaJSON() {
		super();
	}
	
	public Organizacija toOrganizacija(Diskovi diskovi, VirtualneMasine virtualneMasine) throws NotUniqueException{
		ArrayList<Object> resursi = new ArrayList<>();
		for(String res: this.getResursi()){
			if(virtualneMasine.getVirtualneMasine().containsKey(res)){
				resursi.add(virtualneMasine.getVirtualneMasine().get(res));
			}else {
				resursi.add(diskovi.getDiskovi().get(res));
			}
		}
		ArrayList<Korisnik> korisnici = new ArrayList<Korisnik>();
		for(KorisnikJSON k: this.korisnici) {
			korisnici.add(k.toKorisnik());
		}
		Organizacija organizacija = new Organizacija(this.ime,this.opis,this.logo, korisnici, resursi);
		return organizacija;
	}
	
	public OrganizacijaJSON(Organizacija organizacija) {
		super();
		this.ime = organizacija.getIme();
		this.opis = organizacija.getOpis();
		this.logo = organizacija.getLogo();
		ArrayList<KorisnikJSON> korisnici = new ArrayList<KorisnikJSON>();
		for(Korisnik k: organizacija.getKorisnici()) {
			korisnici.add(new KorisnikJSON(k));
		}
		this.korisnici = korisnici;
		ArrayList<String> resursiString = new ArrayList<>();
		for(Object o: organizacija.getResursi()) {
			if(o instanceof Disk) {
				Disk disk = (Disk) o;
				resursiString.add(disk.getIme());
			}else {
				VirtualnaMasina virtualnaMasina = (VirtualnaMasina) o;
				resursiString.add(virtualnaMasina.getIme());
			}
		}
		this.resursi = resursiString;
	}

}
