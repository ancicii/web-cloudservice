package dto;

import beans.Korisnik;
import beans.Organizacija;
import beans.Uloga;

public class AzurirajProfilDTO {
	
	private String email;
	private String ime;
	private String prezime;
	private String lozinka;
	private String lozinka1;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getLozinka() {
		return lozinka;
	}
	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}
	public String getLozinka1() {
		return lozinka1;
	}
	public void setLozinka1(String lozinka1) {
		this.lozinka1 = lozinka1;
	}
	public AzurirajProfilDTO(String email, String ime, String prezime, String lozinka, String lozinka1) {
		super();
		this.email = email;
		this.ime = ime;
		this.prezime = prezime;
		this.lozinka = lozinka;
		this.lozinka1 = lozinka1;
	}
	public AzurirajProfilDTO() {
		super();
	}
	
	public Korisnik toKorisnik(Organizacija organizacija, Uloga uloga) {
		Korisnik korisnik = new Korisnik(this.email, this.lozinka, this.ime, this.prezime, organizacija, uloga);
		return korisnik;
	}
	
	

}
