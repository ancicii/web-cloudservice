package beans;

import java.util.ArrayList;

public class VirtualnaMasina {
	
	private String ime;
	private Kategorija kategorija;
	private ArrayList<Disk> diskovi;
	private ArrayList<Aktivnost> aktivnosti;
	
	public String getIme() {
		return ime;
	}
	public void setIme(String ime){
        this.ime = ime;   
	}
	public Kategorija getKategorija() {
		return kategorija;
	}
	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}
	public ArrayList<Disk> getDiskovi() {
		return diskovi;
	}
	public void setDiskovi(ArrayList<Disk> diskovi) {
		this.diskovi = diskovi;
	}
	public ArrayList<Aktivnost> getAktivnosti() {
		return aktivnosti;
	}
	public void setAktivnosti(ArrayList<Aktivnost> aktivnosti) {
		this.aktivnosti = aktivnosti;
	}
	public VirtualnaMasina(String ime, Kategorija kategorija, ArrayList<Disk> diskovi,
			ArrayList<Aktivnost> aktivnosti){
		super();
		this.ime = ime;
		this.kategorija = kategorija;
		this.diskovi = diskovi;
		this.aktivnosti = aktivnosti;
	}
	public VirtualnaMasina() {
		super();
	}
	public VirtualnaMasina(String ime, Kategorija kategorija, ArrayList<Disk> diskovi) {
		super();
		this.ime = ime;
		this.kategorija = kategorija;
		this.diskovi = diskovi;
		this.aktivnosti = new ArrayList<Aktivnost>();
	}
	
}
