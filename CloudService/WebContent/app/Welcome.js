Vue.component("navigation",{
	data: function(){
		return{
			type: sessionStorage.getItem("tip")
		}
	},
	template:`
		<div class="collapse navbar-collapse" id="navbarResponsive">
		<ul class="navbar-nav ml-auto">
			<li class="nav-item active">
			<router-link class="nav-link" to="/virtualneMasine/pregled" v-if="(type === 'korisnik')||(type === 'admin')||(type === 'superAdmin')">Virtualne Masine
					<span class="sr-only">(current)</span>
			</router-link>
			</li>
			<li>
			<router-link class="nav-link" to="/organizacije/pregled" v-if="type === 'superAdmin'">Organizacije
					<span class="sr-only">(current)</span>
			</router-link>
			</li>
			<li>
			<router-link class="nav-link" to="/korisnici/pregled" v-if="(type === 'admin')||(type === 'superAdmin')">Korisnici
					<span class="sr-only">(current)</span>
			</router-link>
			</li>
			<li>
			<router-link class="nav-link" to="/diskovi/pregled" v-if="(type === 'korisnik')||(type === 'admin')||(type === 'superAdmin')">Diskovi
					<span class="sr-only">(current)</span>
			</router-link>
			</li>
			<li>
			<router-link class="nav-link" to="/kategorije/pregled" v-if="type === 'superAdmin'">Kategorije
					<span class="sr-only">(current)</span>
			</router-link>
			</li>
			<li class="nav-item" v-if="(type === null||type === 'null')">
			<router-link class="nav-link" to="/cloudy/login">Ulogujte se</router-link>
			</li>
			<li class="nav-item" v-if="(type === 'korisnik')||(type === 'admin')||(type === 'superAdmin')">
			<router-link class="nav-link" to="/cloudy/mojProfil">Profil</router-link>
			</li>
			<li class="nav-item" v-if="(type === 'korisnik')||(type === 'admin')||(type === 'superAdmin')">
			<router-link class="nav-link" to="/" @click.native="odjava">Odjavite se</router-link>
			</li>
			
			
		</ul>
		</div>
	`,
	methods:{
		odjava: function(){
			axios.
			get('/cloudy/logout',{ headers: { Authorization: sessionStorage.getItem("Authorization") } })
				.then(response => {
					sessionStorage.setItem("tip",null);
					sessionStorage.setItem("Authorization", null);
					this.type="null";
					router.push('/cloudy/login');
				})
				.catch(error => (alert(error.response.data)));
			
		}
	},
	mounted(){
		console.log(this.type);
		
	}
});