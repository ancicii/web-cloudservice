package dto;

import beans.Korisnik;
import beans.Organizacija;
import beans.Uloga;

public class KorisnikDTO {
	
	private String lozinka;
	private String ime;
	private String prezime;
	private String uloga;
	
	public String getLozinka() {
		return lozinka;
	}
	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getUloga() {
		return uloga;
	}
	public void setUloga(String uloga) {
		this.uloga = uloga;
	}
	public KorisnikDTO(String lozinka, String ime, String prezime, String uloga) {
		super();
		this.lozinka = lozinka;
		this.ime = ime;
		this.prezime = prezime;
		this.uloga = uloga;
	}
	public KorisnikDTO() {
		super();
	}
	public Korisnik toKorisnik(String email, Organizacija organizacija) {
		Korisnik korisnik = new Korisnik(email, this.lozinka, this.ime, this.prezime, organizacija, Uloga.valueOf(this.uloga));
		return korisnik;
	}
	
	

}
