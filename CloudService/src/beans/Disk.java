package beans;

public class Disk {
	private String ime;
	private TipDiska tip;
	private int kapacitet;
	private VirtualnaMasina virtualnaMasina;
	
	public Disk(String ime, TipDiska tip, int kapacitet, VirtualnaMasina virtualnaMasina){
		super();
		this.ime = ime;
		this.tip = tip;
		this.kapacitet = kapacitet;
		this.virtualnaMasina = virtualnaMasina;
	}
	public Disk() {
		super();
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime){
	    this.ime = ime;   
	}
	public TipDiska getTip() {
		return tip;
	}
	public void setTip(TipDiska tip) {
		this.tip = tip;
	}
	public int getKapacitet() {
		return kapacitet;
	}
	public void setKapacitet(int kapacitet) {
		this.kapacitet = kapacitet;
	}
	public VirtualnaMasina getVirtualnaMasina() {
		return virtualnaMasina;
	}
	public void setVirtualnaMasina(VirtualnaMasina virtualnaMasina) {
		this.virtualnaMasina = virtualnaMasina;
	}
	
}