Vue.component("organizacije", {
	data: function () {
		return {
			organizacije: null
		}
	},
	template: `
	<div class="organizacije">
    <div class="container organiz text-dark bg-white mt-5 pb-2 mb-3">
      <div class="row p-4 bg-mobile">
        <div class="col-md-6">
          <h2>Organizacije</h2>
        </div>
        <div class="col-md-6 ">
          <button @click="funct('ne', null)" class="nav-link btn btn-custom pull-right">Dodaj</button>
        </div>
			</div>
			<div class="row p-3 effects" >
		  		<div class="col-md-4" style="min-height:20px;">
						<h3 style="color:#824E71;">LOGO</h3>
				  </div>
				<div class="col-md-4" style="min-height:20px;">
				<h3 style="color:#824E71;">IME</h3>
		  		</div>
		  		<div class="col-md-4" style="min-height:20px;">
        <h3 style="color:#824E71;">OPIS</h3>
      </div>
		</div>
		<hr>
				<div v-for="org in organizacije" @click="funct('da',$event)" class="row p-3 effects" >
		  		<div class="col-md-4 slikaOkvir" style="min-height:90px;">
						<img :src="org.logo" class="img-responsive" >
				  </div>
				<div class="col-md-4" style="min-height:90px;">
				<h3 class="text-info">{{org.ime}}</h3>
		  		</div>
		  		<div class="col-md-4" style="min-height:90px;">
        <p class="text-info text-center">{{org.opis}}</p>
      </div>
	  </div>
	  </div>
	</div>
	`,
	methods: {
		funct: function (w, event) {
			nazivOrganizacije = null;

			if (event != null) {
				parent = event.target.parentElement;
				parentClass = event.target.parentElement.className;
				if (parentClass === "row p-3 effects") {
					nazivOrganizacije = parent.childNodes[2].innerText;
				}
				else {
					parent = parent.parentElement;
					nazivOrganizacije = parent.childNodes[2].innerText;
				}
			}
			sessionStorage.setItem("izmeniO", w);
			sessionStorage.setItem("nazivOrganizacije", nazivOrganizacije);
			router.push('/organizacije/mod');
		}
	},
	mounted() {
		axios.
			get('/organizacije/pregled', { headers: { Authorization: sessionStorage.getItem("Authorization") } })
			.then(response => {
				this.organizacije = response.data;
			});
	}


})