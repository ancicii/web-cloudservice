package exceptions;

@SuppressWarnings("serial")
public class BadFileFormat extends Exception {

	public BadFileFormat(String mess) {
		super(mess);
	}
	
	
}
