package beans;

import java.time.OffsetDateTime;

public class Aktivnost {
	
	private OffsetDateTime datumUpaljena;
	private OffsetDateTime datumUgasena;
	
	public OffsetDateTime getDatumUpaljena() {
		return datumUpaljena;
	}
	public void setDatumUpaljena(OffsetDateTime datumUpaljena) {
		this.datumUpaljena = datumUpaljena;
	}
	public OffsetDateTime getDatumUgasena() {
		return datumUgasena;
	}
	public void setDatumUgasena(OffsetDateTime datumUgasena) {
		this.datumUgasena = datumUgasena;
	}
	public Aktivnost(OffsetDateTime datumUpaljena, OffsetDateTime datumUgasena) {
		super();
		this.datumUpaljena = datumUpaljena;
		this.datumUgasena = datumUgasena;
	}
	public Aktivnost() {
		super();
	}

}
