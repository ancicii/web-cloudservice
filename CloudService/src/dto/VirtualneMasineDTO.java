package dto;

import beans.Kategorija;
import beans.Organizacija;

public class VirtualneMasineDTO {
	
	private String ime;
	private Kategorija kategorija;
	private Organizacija organizacija;
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public Kategorija getKategorija() {
		return kategorija;
	}
	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}
	public Organizacija getOrganizacija() {
		return organizacija;
	}
	public void setOrganizacija(Organizacija organizacija) {
		this.organizacija = organizacija;
	}
	public VirtualneMasineDTO(String ime, Kategorija kategorija, Organizacija organizacija) {
		super();
		this.ime = ime;
		this.kategorija = kategorija;
		this.organizacija = organizacija;
	}
	public VirtualneMasineDTO() {
		super();
	}
	
	

}
