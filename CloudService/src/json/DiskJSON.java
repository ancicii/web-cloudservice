package json;

import beans.Disk;
import beans.TipDiska;

public class DiskJSON {
	private String ime;
	private String tip;
	private int kapacitet;
	
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getTip() {
		return tip;
	}
	public void setTip(String tip) {
		this.tip = tip;
	}
	public int getKapacitet() {
		return kapacitet;
	}
	public void setKapacitet(int kapacitet) {
		this.kapacitet = kapacitet;
	}

	public DiskJSON(String ime, String tip, int kapacitet) {
		super();
		this.ime = ime;
		this.tip = tip;
		this.kapacitet = kapacitet;
	}
	public DiskJSON() {
		super();
	}
	
	public Disk toDisk(){
		Disk disk = new Disk(this.ime, TipDiska.valueOf(this.tip), this.kapacitet, null);
		return disk;
	}
	
}
