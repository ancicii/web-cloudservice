Vue.component("dodaj-izmeni-virtualnu-masinu", {
	data: function () {
		return {
			virtualnaMasina: {
				ime: "",
				kategorija: {
					ime: "",
					gpujezgra: "",
					brojJezgara: "",
					ram: ""
				},
				diskovi: "",
				aktivnosti: [],
				organizacija: ""
			},
			izmeni: sessionStorage.getItem("izmeniVM"),
			imeVMZaIzmenu: sessionStorage.getItem("imeVM"),
			uloga: sessionStorage.getItem("tip"),
			kategorije: null,
			organizacije: null,
			organizacijaAdmina: null,
			diskovi: null,
			selected: [],
			cc: [],
			broj: 0
		}
	},
	template: `
<div class="organizacije1">
    <div class="container cntBg text-dark pt-5 pb-5 mb-2 bootstrap snippet">
        <div class="row" v-if="izmeni=='da' && uloga!='korisnik'">
            <div class="col-lg-12" >
                <button @click="obrisi" class="nav-link btn btn-custom float-right" title="Obrisi virtualnu masinu"><i class="fa fa-user-times fa-2x" style="color:#6C1539;"></i></button>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <form class="form" action="##" method="post" id="organizacijaForm">
                    <div class="form-group">
                        <div class="col-xs-6">
                            <label for="ime">
                                <h4 class="font">Ime</h4></label>
							<input type="text" disabled style="background: #ffffff;color:#000000;" v-if="uloga=='korisnik'" v-model="virtualnaMasina.ime" class="form-control" name="ime" id="ime" placeholder="ime virtualne masine" title="Unesite ime virtualne masine">
							<input type="text" v-if="uloga!='korisnik'" v-model="virtualnaMasina.ime" class="form-control" name="ime" id="ime" placeholder="ime virtualne masine" title="Unesite ime virtualne masine">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-6">
                            <label for="kategorija">
                                <h4 class="font">Kategorija</h4></label>
                            <select v-if="uloga!='korisnik'" @change="onChange($event)" style="font-family: Montserrat; font-size:14px" id="kategorija" class="form-control" v-model="virtualnaMasina.kategorija.ime">
                                <option value="" disabled selected>Izaberite kategoriju</option>
                                <option v-for="(kat, index) in kategorije" :key="index" :value="kat.ime">{{ kat.ime }}</option>
							</select>
							<select disabled v-if="uloga=='korisnik'" style="background: #ffffff;color:#000000;font-family: Montserrat; font-size:14px" @change="onChange($event)" id="kategorija" class="form-control" v-model="virtualnaMasina.kategorija.ime">
                                <option value="" disabled selected>Izaberite kategoriju</option>
                                <option v-for="(kat, index) in kategorije" :key="index" :value="kat.ime">{{ kat.ime }}</option>
                            </select>
						</div>
						
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg">
                                <label for="brojJezgara">
                                    <h4 class="font">Broj jezgara</h4></label>
                                <input type="text" disabled style="background: #ffffff;color:#000000;" v-model="virtualnaMasina.kategorija.brojJezgara" class="form-control" name="brojJezgara" id="brojJezgara" placeholder="broj jezgara">
                            </div>
                            <div class="col-lg">
                                <label for="ram">
                                    <h4 class="font">RAM</h4></label>
                                <input type="text" disabled style="background: #ffffff;color:#000000;" v-model="virtualnaMasina.kategorija.ram" class="form-control" name="ram" id="ram" placeholder="RAM">
                            </div>
                            <div class="col-lg">
                                <label for="gpu">
                                    <h4 class="font">GPU</h4></label>
                                <input type="text" style="background: #ffffff;color:#000000;" disabled v-model="virtualnaMasina.kategorija.gpujezgra" class="form-control" name="gpu" id="gpu" placeholder="GPU">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-6">
                            <label for="organizacija">
                                <h4 class="font">Organizacija</h4></label>
                            <select @change="onChangeOrg($event)" v-if="izmeni=='ne'&& uloga=='superAdmin'" style="font-family: Montserrat; font-size:14px" id="organizacija" class="form-control" v-model="virtualnaMasina.organizacija">
                                <option value="" disabled selected>Izaberite organizaciju</option>
                                <option v-for="(org, index) in organizacije" :key="index" :value="org.ime">{{ org.ime }}</option>
                            </select>
                            <select v-if="izmeni=='ne' && uloga=='admin'" disabled style="background: #ffffff;color:#000000;font-family: Montserrat; font-size:14px" id="organizacija" class="form-control" v-model="organizacijaAdmina">
                                <option selected>{{organizacijaAdmina}}</option>
                            </select>
                            <select v-if="izmeni=='da'" disabled style="background: #ffffff;color:#000000;font-family: Montserrat; font-size:14px" id="organizacija" class="form-control" v-model="virtualnaMasina.organizacija.ime">
                                <option selected>{{virtualnaMasina.organizacija.ime}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="container organiz text-dark bg-white mt-5 pb-2 mb-3">
                            <div class="row bg-mobile">
                                <div class="col">
                                    <h4 class="font" style="text-align: left;">Diskovi</h4>
                                </div>
                            </div>
                            <div class="row effects">
                                <div class="col-xs" style="margin-left:170px;" v-if="izmeni=='ne'">
                                    <h6 style="color:#824E71;"></h6>
								</div>
								<div class="col-xs" style="margin-left:130px;" v-if="izmeni=='da'">
                                    <h6 style="color:#824E71;">IME</h6>
                                </div>
                                <div class="col-md">
                                    <h6 style="color:#824E71;" v-if="izmeni=='ne'">IME</h6>
                                </div>
                                <div class="col-md" style="margin-left:45px;" v-if="izmeni=='da'">
                                    <h6 style="color:#824E71;">TIP</h6>
								</div>
								<div class="col-md" style="margin-left:115px;" v-if="izmeni=='ne'">
                                    <h6 style="color:#824E71;">TIP</h6>
                                </div>
                                <div class="col-md" style="margin-left:50px;" v-if="izmeni=='da'">
                                    <h6 style="color:#824E71;">KAPACITET</h6>
								</div>
								<div class="col-md" style="margin-left:90px;" v-if="izmeni=='ne'">
                                    <h6 style="color:#824E71;">KAPACITET</h6>
                                </div>
                            </div>
                            <hr>
                            <div v-for="disk in diskovi" class="row effects">
                                <div class="col-xs form-check" v-if="izmeni=='ne'">
                                    <input class="form-check-input" style="margin-top:25px;" type="checkbox" :value="disk" v-model="selected">
                                </div>
                                <div class="col-md" style="min-height:90px;">
                                    <p class="text-info text-center">{{disk.ime}}</p>
                                </div>
                                <div class="col-md" style="min-height:90px;">
                                    <p class="text-info text-center">{{disk.tip}}</p>
                                </div>
                                <div class="col-md" style="min-height:90px;">
                                    <p class="text-info text-center">{{disk.kapacitet}}</p>
                                </div>
                            </div>
                        </div>
					</div>
					<div class="form-group" v-if="izmeni=='da'">
                        <div class="container organiz text-dark bg-white mt-5 pb-2 mb-3">
                            <div class="row bg-mobile">
                                <div class="col">
                                    <h4 class="font" style="text-align: left;">Aktivnosti</h4>
                                </div>
                            </div>
                            <div class="row effects">                                
								<div class="col-md-4">
                                    <h6 style="color:#824E71;margin-left:105px;" v-if="uloga=='superAdmin'">POCETAK</h6>
								</div>
								<div class="col">
                                    <h6 style="color:#824E71;margin-left:-115px;" v-if="uloga!='superAdmin'">POCETAK</h6>
                                </div>
                                <div class="col-md-4">
                                    <h6 style="color:#824E71;margin-left:115px;" v-if="uloga=='superAdmin'">KRAJ</h6>
								</div>
								<div class="col">
                                    <h6 style="color:#824E71;margin-left:-115px;" v-if="uloga!='superAdmin'">KRAJ</h6>
                                </div>
                                <div class="col-xs-2" style="margin-right:105px;" v-if="uloga=='superAdmin'">
                                    <h6 style="color:#824E71;">IZMENI</h6>
								</div>
								<div class="col-xs-2" style="margin-right:75px;" v-if="uloga=='superAdmin'">
                                    <h6 style="color:#824E71;">OBRISI</h6>
                                </div>                                
                            </div>
                            <hr>
                            <div v-for="(ak,index) in cc" class="row effects">
								<div class="col-md-4" v-if="uloga=='superAdmin'">
									<input type="datetime-local" v-if="ak.upaljena!=='1970-01-01T01:00'" :id="'datePickerUpaljena'+index" style="margin-top:20px;" name="datumUpaljena" v-model="ak.upaljena" class="form-control">
									<input type="datetime-local" v-if="ak.upaljena==='1970-01-01T01:00'" :id="'datePickerUpaljena'+index" style="margin-top:20px;" name="datumUpaljena" class="form-control">
								</div>
								<div class="col" v-if="uloga!='superAdmin'">
									<input type="datetime-local" disabled style="background: #ffffff;color:#000000;margin-top:20px;" v-if="ak.upaljena!=='1970-01-01T01:00'" :id="'datePickerUpaljena'+index" name="datumUpaljena" v-model="ak.upaljena" class="form-control">
									<input type="datetime-local" disabled style="background: #ffffff;color:#000000;margin-top:20px;" v-if="ak.upaljena==='1970-01-01T01:00'" :id="'datePickerUpaljena'+index" name="datumUpaljena" class="form-control">
							    </div>
								<div class="col-md-4" v-if="uloga=='superAdmin'">
                                    <input type="datetime-local" v-if="ak.ugasena!=='1970-01-01T01:00'" :id="'datePickerUgasena'+index" style="margin-top:20px;" name="datumUgasena" v-model="ak.ugasena" class="form-control">
									<input type="datetime-local" v-if="ak.ugasena==='1970-01-01T01:00'" :id="'datePickerUgasena'+index" style="margin-top:20px;" name="datumUgasena" class="form-control">
								</div>
								<div class="col" v-if="uloga!='superAdmin'">
                                    <input type="datetime-local" disabled style="background: #ffffff;color:#000000;margin-top:20px;" v-if="ak.ugasena!=='1970-01-01T01:00'" :id="'datePickerUgasena'+index" name="datumUgasena" v-model="ak.ugasena" class="form-control">
									<input type="datetime-local" disabled style="background: #ffffff;color:#000000;margin-top:20px;" v-if="ak.ugasena==='1970-01-01T01:00'" :id="'datePickerUgasena'+index" name="datumUgasena" class="form-control">
								</div>
                                <div class="col-xs" v-if="uloga=='superAdmin'" :id="'Izmeni'+index" style="margin-left:60px;">
        							<p @click="izmeniAktivnost($event,'ne', 'ne')" class="nav-link"><i class="fa fa-edit"></i></p>
        						</div>
        						<div class="col-xs" v-if="uloga=='superAdmin'" :id="'Obrisi'+index" style="margin-left:80px;">
        							<p @click="izmeniAktivnost($event,'da', 'ne')" class="nav-link"><i class="fa fa-archive"></i></i></p>
          						</div>
                            </div>
                        </div>
					</div>
					<div class="form-group">
                        <div class="row">
                            <div class="col" v-if="uloga!='korisnik'">
                                <input type="button" class="orgBtn" v-if="izmeni === 'ne'" value="Dodaj" @click="dodajVirtualnuMasinu">
                                <input type="button" class="orgBtn" v-if="izmeni === 'da'" value="Izmeni" @click="izmeniVirtualnuMasinu($event)">
                            </div>
                            <div class="col">
                                <input type="button" class="orgBtn" @click="nazad" value="Nazad">
                            </div>
                        </div>
                    </div>
                </form>

            </div>
            <!--/col-9-->

        </div>
        <!--/row-->

    </div>
</div>
	
	`,
	methods: {
		onChange: function (event) {
			axios.
				get('/kategorije/get/' + event.target.value, { headers: { Authorization: sessionStorage.getItem("Authorization") } })
				.then(response => {
					this.virtualnaMasina.kategorija = response.data;
				});
		},
		onChangeOrg: function (event) {
			this.organizacijaAdmina = event.target.value;
			this.nadjiDiskove();
		},
		getSveDatume: function () {
			var elementCollection = new Array();
			var allElements = document.getElementsByTagName("*");
			var broj = 0;
			for (i = 0; i < allElements.length; i++) {
				if (allElements[i].id.includes("datePickerUpaljena")) {
					broj++;
				}				
			}
			return broj;
		},
		izmeniAktivnost: function (event, obrisi, izmenaCele) {
			var obrisiAktivnost = null;
			if(obrisi === 'da'){
				obrisiAktivnost = event.target.parentElement.parentElement.id.split("Obrisi")[1];
			}
			var sveAktivnosti = new Array();
			for (var i = 0; i < this.getSveDatume(); i++) {					
				if(obrisiAktivnost!==null && obrisiAktivnost===i.toString()){
					continue;
				}		
				var aktivnost = {
					datumUpaljena: $('#datePickerUpaljena' + i.toString()).val(),
					datumUgasena: $('#datePickerUgasena' + i.toString()).val()
				}
				sveAktivnosti.push(aktivnost);
				
			}
			console.log(sveAktivnosti);
			axios
				.post('/virtualneMasine/izmeniAktivnosti/'+this.imeVMZaIzmenu, sveAktivnosti, { headers: { Authorization: sessionStorage.getItem("Authorization") } })
				.then(response => {
					if (response.status === 200) {
						if(izmenaCele ==='ne'){
							router.go();	
						}						
					};
				}).catch(error => { alert(error.response.data) });			
		},
		nazad: function () {
			router.push('/virtualneMasine/pregled')
		},
		dodajVirtualnuMasinu: function () {
			let vm = {
				ime: this.virtualnaMasina.ime,
				kategorija: this.virtualnaMasina.kategorija,
				diskovi: this.selected,
				organizacija: this.virtualnaMasina.organizacija
			};
			axios
				.post('/virtualneMasine/dodaj', vm, { headers: { Authorization: sessionStorage.getItem("Authorization") } })
				.then(response => {
					if (response.status === 200) {
						router.push('/virtualneMasine/pregled')
					};
				}).catch(error => { alert(error.response.data) });
		},
		nadjiVirtualnuMasinu: function () {
			axios
				.get('/virtualneMasine/get/' + this.imeVMZaIzmenu, { headers: { Authorization: sessionStorage.getItem("Authorization") } })
				.then(response => {
					Array.from(response.data.aktivnosti).forEach(akt => {
						var upaljenaD = new Date(akt.datumUpaljena);
						var ugasenaD = new Date(akt.datumUgasena);
						var upaljenaSati = upaljenaD.getHours();
						var upaljenaMinuti = upaljenaD.getMinutes();
						var ugasenaSati = ugasenaD.getHours();
						var ugasenaMinuti = ugasenaD.getMinutes();
						if (upaljenaSati.toString.length < 2) {
							upaljenaSati = '0' + upaljenaSati;
						}
						if (upaljenaMinuti.toString.length < 2) {
							upaljenaMinuti = '0' + upaljenaMinuti;
						}
						if (ugasenaSati.toString.length < 2) {
							ugasenaSati = '0' + ugasenaSati;
						}
						if (ugasenaMinuti.toString.length < 2) {
							ugasenaMinuti = '0' + ugasenaMinuti;
						}
						var aktivnost = {

							upaljena: upaljenaD.toISOString().split('T')[0] + "T" + upaljenaSati + ":" + upaljenaMinuti,
							ugasena: ugasenaD.toISOString().split('T')[0] + "T" + ugasenaSati + ":" + ugasenaMinuti
						}
						this.cc.push(aktivnost);
					})
					this.virtualnaMasina.ime = response.data.ime;
					this.virtualnaMasina.kategorija = response.data.kategorija;
					this.virtualnaMasina.diskovi = response.data.diskovi;
					this.virtualnaMasina.organizacija = response.data.organizacija;
					this.virtualnaMasina.aktivnosti = response.data.aktivnosti;
					this.diskovi = response.data.diskovi;
				})
				.catch(error => { alert(error) });
		},
		izmeniVirtualnuMasinu: function (event) {
			let vm = {
				ime: this.virtualnaMasina.ime,
				kategorija: this.virtualnaMasina.kategorija
			};
			this.izmeniAktivnost(event,'ne','da');
			axios
				.put('/virtualneMasine/izmeni/' + this.imeVMZaIzmenu, vm, { headers: { Authorization: sessionStorage.getItem("Authorization") } })
				.then(response => {
					if (response.status === 200) {
						router.push('/virtualneMasine/pregled')
					};
				}).catch(error => { alert(error.response.data) });
		},
		obrisi: function () {
			axios.delete('/virtualneMasine/obrisi/' + this.imeVMZaIzmenu, { headers: { Authorization: sessionStorage.getItem("Authorization") } })
				.then(response => {
					if (response.status === 200) {
						router.push('/virtualneMasine/pregled')
					};
				}).catch(error => { alert(error.response.data) });
		},
		nadjiDiskove: function () {
			axios
				.get('/diskovi/get', { headers: { Authorization: sessionStorage.getItem("Authorization") } })
				.then(response => {
					if (response.status === 200) {
						this.diskovi = response.data;
					};
				}).catch(error => { alert(error.response.data) });

		}
	},
	mounted() {
		if (sessionStorage.getItem("tip") === "superAdmin") {
			axios.
				get('/organizacije/pregled', { headers: { Authorization: sessionStorage.getItem("Authorization") } })
				.then(response => {
					this.organizacije = response.data;
				});
		} else if(sessionStorage.getItem("tip") === "admin"){
			axios
				.get('/korisnici/getOrganizacija', { headers: { Authorization: sessionStorage.getItem("Authorization") } })
				.then(response => {
					if (response.status === 200) {
						this.organizacijaAdmina = response.data;
						this.virtualnaMasina.organizacija = response.data;
						if (sessionStorage.getItem("tip") === "admin"&&this.izmeni==="ne") {
							this.nadjiDiskove();
						}
					};
				}).catch(error => { alert(error.response.data) });
		}
		axios
			.get('/kategorije/pregled', { headers: { Authorization: sessionStorage.getItem("Authorization") } })
			.then(response => {
				if (response.status === 200) {
					this.kategorije = response.data;
				};
			}).catch(error => { alert(error.response.data) });

		if (this.izmeni === "da") {
			this.nadjiVirtualnuMasinu();
		}
	}


});