package beans;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class Kategorije {
	
	private HashMap<String, Kategorija> kategorije = new HashMap<String, Kategorija>();

	public HashMap<String, Kategorija> getKategorije() {
		return kategorije;
	}

	public void setKategorije(HashMap<String, Kategorija> kategorije) {
		this.kategorije = kategorije;
	}

	public Kategorije() {
		super();
		
		String line;
		String data = "";
		Gson gson = new Gson();
		try {
			File file = new File("D:\\Ana\\Web Projekat\\web-cloudservice\\CloudService\\WebContent\\resources\\kategorije.txt");
			BufferedReader br = new BufferedReader(new FileReader(file));
			try {
				while ((line = br.readLine())!=null){
					data += line;
				}
				kategorije = gson.fromJson(data, new TypeToken<HashMap<String, Kategorija>>(){}.getType()); 
			}catch(IOException e) {
				e.printStackTrace();
			}
			 br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	

}
