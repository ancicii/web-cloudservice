package dto;

import beans.Organizacija;

public class OrganizacijaDTO {
	
	private String ime;
	private String opis;
	private String logo;
	
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public OrganizacijaDTO() {
		super();
	}
	public OrganizacijaDTO(String ime, String opis, String logo) {
		super();
		this.ime = ime;
		this.opis = opis;
		this.logo = logo;
	}
	
	public Organizacija toOrganizacija() {
		Organizacija organizacija = new Organizacija(this.ime, this.opis, this.logo);
		return organizacija;
	}

}
