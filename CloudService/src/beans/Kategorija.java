package beans;

public class Kategorija {
	private String ime;
	private int brojJezgara;
	private int RAM; 
	private int GPUJezgra;
	
	public Kategorija() {
		super();
	}
	public Kategorija(String ime, int brojJezgara, int rAM, int gPUJezgra){
		super();
		this.ime = ime;
		this.brojJezgara = brojJezgara;
		this.RAM = rAM;
		this.GPUJezgra = gPUJezgra;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime){
        this.ime = ime;   
	}
	public int getBrojJezgara() {
		return brojJezgara;
	}
	public void setBrojJezgara(int brojJezgara) {
		this.brojJezgara = brojJezgara;
	}
	public int getRAM() {
		return RAM;
	}
	public void setRAM(int rAM) {
		this.RAM = rAM;
	}
	public int getGPUJezgra() {
		return GPUJezgra;
	}
	public void setGPUJezgra(int gPUJezgra) {
		this.GPUJezgra = gPUJezgra;
	}

}
