package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Kategorija;
import dto.KategroijeMaxDTO;

@DeclareRoles({ "SUPER_ADMIN", "ADMIN", "KORISNIK" })
@Path("/kategorije")
public class ServiceKategorije {

	CloudService cs = new CloudService();

	@GET
	@RolesAllowed({ "SUPER_ADMIN", "ADMIN", "KORISNIK" })
	@Path("/pregled")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Kategorija> getKorisnici() {
		Collection<Kategorija> list = new ArrayList<Kategorija>();
		list = cs.kategorije.getKategorije().values();
		return list;
	}

	@GET
	@RolesAllowed({ "SUPER_ADMIN", "ADMIN", "KORISNIK" })
	@Path("/get/{ime}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getKategorija(@PathParam("ime") String ime) {
		Kategorija kategorija = null;
		if (cs.kategorije.getKategorije().containsKey(ime)) {
			kategorija = cs.kategorije.getKategorije().get(ime);
			return Response.status(Response.Status.OK).entity(kategorija).build();

		} else {
			return Response.status(Response.Status.BAD_REQUEST).entity("Greska").build();

		}
	}

}
