Vue.component("log-in", {
    data: function () {
        return {
            input: {
                email: "",
                lozinka: ""
            }
        }
    },

    template: `
    <div>
        <div class="logo"></div>
        <div class="login-block">
            <h1 class="h1c">Uloguj se</h1>
            <input type="text" v-model="input.email" value="" placeholder="Unesite email" id="email" />
            <input type="password" v-model="input.lozinka" value="" placeholder="Unesite lozinku" id="lozinka" />
            <button @click="login(input)">Prijava</button>
        </div> 
    </div>
    `
    ,
    methods: {
        login: function (input) {
            const koris = {
                email: input.email,
                lozinka: input.lozinka
            };
            sessionStorage.setItem("Authorization", btoa(koris.email + ":" + koris.lozinka));
            axios
                .post('/cloudy/login', koris, { headers: { Authorization: sessionStorage.getItem("Authorization") } })
                .then(response => (this.init(response, koris)))
                .catch(error => (alert(error.response.data)));
        },
        init: function (response, koris) {
            if (response.data === "ADMIN") {
                sessionStorage.setItem("tip", "admin");
                sessionStorage.setItem("korisnik", koris.email);
                router.go();
            } else if (response.data === "KORISNIK") {
                sessionStorage.setItem("tip", "korisnik")
                sessionStorage.setItem("korisnik", koris.email);
                router.go();
            } else {
                sessionStorage.setItem("tip", "superAdmin");
                sessionStorage.setItem("korisnik", koris.email);
                router.go();
            }

        }

    },
    beforeMount() {
        if (sessionStorage.getItem("tip") === "admin" || sessionStorage.getItem("tip") === "superAdmin" || sessionStorage.getItem("tip") === "korisnik") {
            router.push('/virtualneMasine/pregled');
        }
    }



});

