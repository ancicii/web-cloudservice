package beans;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import json.VirtualnaMasinaJSON;

public class VirtualneMasine {
	
	private HashMap<String, VirtualnaMasina> virtualneMasine = new HashMap<String, VirtualnaMasina>();

	public HashMap<String, VirtualnaMasina> getVirtualneMasine() {
		return virtualneMasine;
	}

	public void setVirtualneMasine(HashMap<String, VirtualnaMasina> virtualneMasine) {
		this.virtualneMasine = virtualneMasine;
	}

	public VirtualneMasine() {
		super();
		
	}

	public VirtualneMasine(Diskovi diskovi) {
		super();
		
		String line;
		String data = "";
		Gson gson = Converters.registerOffsetDateTime(new GsonBuilder()).create();
		
		HashMap<String, VirtualnaMasinaJSON> virtualneMasineJSON = new HashMap<String, VirtualnaMasinaJSON>();
		try {
			File file = new File("D:\\Ana\\Web Projekat\\web-cloudservice\\CloudService\\WebContent\\resources\\virtualneMasine.txt");
			BufferedReader br = new BufferedReader(new FileReader(file));
			try {
				while ((line = br.readLine())!=null){
					data += line;
				}
				
				virtualneMasineJSON = gson.fromJson(data, new TypeToken<HashMap<String, VirtualnaMasinaJSON>>(){}.getType());
				for(VirtualnaMasinaJSON vmj: virtualneMasineJSON.values()){
					virtualneMasine.put(vmj.getIme(), vmj.toVirtualnaMasina());
				}
				for(VirtualnaMasina vm : virtualneMasine.values()){
					for(Disk disk : vm.getDiskovi()){
						diskovi.getDiskovi().get(disk.getIme()).setVirtualnaMasina(vm);
					}
				}
				
			}catch(IOException e) {
				e.printStackTrace();
			}
			 br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

}
