Vue.component("dodaj-izmeni-disk", {
	data: function(){
		return {
			disk: {
				ime: "",
				tip: "",
				kapacitet: "",
				virtualnaMasina: ""				
			},
			izmeni: sessionStorage.getItem("izmeniDisk"),
			imeZaIzmenu: sessionStorage.getItem("imeDiska"),
			uloga: sessionStorage.getItem("tip"),
			organizacija: "",
			virtualneMasine:""
		}
	},
	template: `
<div class="organizacije1">
    <div class="container cntBg text-dark pt-5 pb-5 mb-2 bootstrap snippet">
        <div class="row" v-if="izmeni=='da'">
            <div class="col-lg-12">
                <button @click="obrisi" class="nav-link btn btn-custom float-right" title="Obrisi disk"><i class="fa fa-user-times fa-2x" style="color:#6C1539;"></i></button>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <form class="form" action="##" method="post" id="organizacijaForm">
                    <div class="form-group">
                        <div class="col-xs-6">
                            <label for="ime">
                                <h4 class="font">Ime</h4></label>
                            <input type="text" v-model="disk.ime" class="form-control" name="ime" id="ime" placeholder="ime diska" title="Unesite ime diska">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-6">
                            <label for="tip">
                                <h4 class="font">Tip</h4></label>
                            <select style="font-family: Montserrat; font-size:14px" id="tip" class="form-control" v-model="disk.tip">
                                <option value="" disabled selected>Izaberite tip diska</option>
                                <option value="SSD">SSD</option>
                                <option value="HDD">HDD</option>
                            </select>
                        </div>
					</div>
					<div class="form-group">
                        <div class="col-xs-6">
                            <label for="kapacitet">
                                <h4 class="font">Kapacitet</h4></label>
                            <input type="number" v-model="disk.kapacitet" class="form-control" name="kapacitet" id="kapacitet" placeholder="kapacitet diska" title="Unesite kapacitet diska">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-6">
                            <label for="vm">
                                <h4 class="font">Virtualna masina</h4></label>
                            <select v-if="izmeni=='ne'"style=" font-family: Montserrat; font-size:14px " id="vm " class="form-control " v-model="disk.virtualnaMasina ">
                                <option value="" disabled selected>Izaberite virtualnu masinu</option>
                                <option v-for="(virt, index) in virtualneMasine" :key="index " :value="virt.ime ">{{ virt.ime }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group " v-if="izmeni=='da' ">
                        <div class="col-xs-6 ">
                            <label for="organizacija ">
                                <h4 class="font ">Organizacija</h4></label>
                            <input type="text " disabled v-model="organizacija " class="form-control " name="organizacija " id="organizacija ">
                        </div>
					</div> 
					<div class="form-group">
                        <div class="row">
                            <div class="col">
                                <input type="button" class="orgBtn" v-if="izmeni === 'ne'" value="Dodaj" @click="dodajDisk">
                                <input type="button" class="orgBtn" v-if="izmeni === 'da'" value="Izmeni" @click="izmeniDisk">
                            </div>
                            <div class="col">
                                <input type="button" class="orgBtn" @click="nazad" value="Nazad">
                            </div>
                        </div>
                    </div>               

                </form>

            </div>
            <!--/col-9-->

        </div>
        <!--/row-->

    </div>
</div>
	`,
methods: { 
    nazad: function(){
    	router.push('/diskovi/pregled');
    },
    dodajDisk: function(){
    	let disk = {
    			ime: this.disk.ime,
    			tip: this.disk.tip,
    			kapacitet: this.disk.kapacitet,
    			virtualnaMasina: this.disk.virtualnaMasina
		};
		console.log(disk);
    	axios
    	.post('/diskovi/dodaj', disk, { headers: { Authorization: sessionStorage.getItem("Authorization") } })
    	.then(response =>{
    		if(response.status === 200){
    			router.push('/diskovi/pregled')
    		};
    	}).catch(error => {alert(error.response.data)});
    },
    nadjiDisk: function(){
    	axios
    	.get('/korisnici/get/'+this.emailZaIzmenu, { headers: { Authorization: sessionStorage.getItem("Authorization") } })
    	 .then(response=>{
				this.korisnik.email = response.data.email;
	    	this.korisnik.ime = response.data.ime;
	    	this.korisnik.prezime = response.data.prezime;
	    	this.korisnik.lozinka = response.data.lozinka;
				this.korisnik.organizacija = response.data.organizacija.ime;
				this.korisnik.uloga = response.data.uloga;
    	 })
    	.catch(error=>{alert(error.response.data)});
    },
    izmeniDisk: function(){
    	let kor = {
    			ime: this.korisnik.ime,
    			prezime: this.korisnik.prezime,
					lozinka: this.korisnik.lozinka,
					uloga: this.korisnik.uloga
    	};
    	axios
    	.put('/korisnici/izmeni/' + this.emailZaIzmenu, kor, { headers: { Authorization: sessionStorage.getItem("Authorization") } })
    	.then(response =>{
    		if(response.status === 200){
    			router.push('/korisnici/pregled')
    		};
    	}).catch(error => {alert(error.response.data)});
		},
		obrisi: function(){
			axios.delete('/korisnici/obrisi/' + this.emailZaIzmenu, { headers: { Authorization: sessionStorage.getItem("Authorization") } })
			.then(response=>{
				if(response.status === 200){
					router.push('/korisnici/pregled')
				};
			}).catch(error=>{alert(error.response.data)});
		}
	},	
  mounted(){
	  if(sessionStorage.getItem("tip")!=="korisnik" && sessionStorage.getItem("izmeniDisk")==='ne'){
		  axios.
			get('/virtualneMasine/pregled', { headers: { Authorization: sessionStorage.getItem("Authorization") } })
			.then(response => {
				this.virtualneMasine = response.data;
			});  
		}else{
			axios
				.get('/korisnici/getOrganizacija', { headers: { Authorization: sessionStorage.getItem("Authorization") } })
				.then(response =>{
					if(response.status === 200){
						console.log(response.data);
						this.organizacijaAdmina = response.data;
						this.korisnik.organizacija = response.data;
					};
				}).catch(error => {alert(error.response.data)});
		}
	  if(this.izmeni==="da"){
		  this.nadjiKorisnika();
		}
	}


});