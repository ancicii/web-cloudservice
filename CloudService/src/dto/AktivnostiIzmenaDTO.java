package dto;

public class AktivnostiIzmenaDTO {
	private String datumUgasena;
	private String datumUpaljena;
	public String getDatumUgasena() {
		return datumUgasena;
	}
	public void setDatumUgasena(String datumUgasena) {
		this.datumUgasena = datumUgasena;
	}
	public String getDatumUpaljena() {
		return datumUpaljena;
	}
	public void setDatumUpaljena(String datumUpaljena) {
		this.datumUpaljena = datumUpaljena;
	}
	public AktivnostiIzmenaDTO(String datumUgasena, String datumUpaljena) {
		super();
		this.datumUgasena = datumUgasena;
		this.datumUpaljena = datumUpaljena;
	}
	public AktivnostiIzmenaDTO() {
		super();
	}
	
	
	
}
