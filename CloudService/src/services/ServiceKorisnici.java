package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import beans.Korisnik;
import beans.Organizacija;
import dto.AzurirajProfilDTO;
import dto.DodajKorisnikaDTO;
import dto.KorisnikDTO;

@DeclareRoles({
    "SUPER_ADMIN",
    "ADMIN",
    "KORISNIK"
})
@Path("/korisnici")
public class ServiceKorisnici {

	@Context
    SecurityContext securityContext;
	CloudService cs = new CloudService();
	
	@GET
	@RolesAllowed({"SUPER_ADMIN", "ADMIN"})
	@Path("/pregled")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Korisnik> getKorisnici() {
		Collection<Korisnik> list = new ArrayList<Korisnik>();
		Korisnik korisnik =(Korisnik) securityContext.getUserPrincipal();
		if(securityContext.isUserInRole("SUPER_ADMIN")) {	
			list = cs.korisnici.getKorisnici().values();			
			list.remove(cs.korisnici.getKorisnici().get(korisnik.getEmail()));			
		}
		else {
			Organizacija organizacija = cs.korisnici.getKorisnici().get(korisnik.getEmail()).getOrganizacija();
			for(Korisnik kor: cs.korisnici.getKorisnici().values()) {
				if(kor.getUloga().toString().equals("SUPER_ADMIN")){
					continue;
				}
				if(kor.getOrganizacija().equals(organizacija)) {
					list.add(kor);
				}
			}
		}
		return list;
	}
	
	@POST
	@RolesAllowed({"SUPER_ADMIN", "ADMIN"})
	@Path("/dodaj")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response dodajKorisnika(DodajKorisnikaDTO korisnik) {
		if (korisnik.getEmail().length() == 0 || korisnik.getIme().length() == 0 || korisnik.getPrezime().length() == 0
				 || korisnik.getLozinka().length() == 0) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Morate popuniti sva polja").build();
		} else if (korisnik.getEmail() == null || korisnik.getIme() == null || korisnik.getPrezime() == null
				 || korisnik.getLozinka() == null || korisnik.getOrganizacija() == null || korisnik.getUloga() == null) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Morate popuniti sva polja").build();
		}
		if (cs.korisnici.getKorisnici().containsKey(korisnik.getEmail())) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Vec postoji korisnik sa unetim email-om").build();
		}
		if(!cs.organizacije.getOrganizacije().containsKey(korisnik.getOrganizacija())) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Organizacija sa izabranim imenom ne postoji!").build();
		}
		Organizacija organizacija = cs.organizacije.getOrganizacije().get(korisnik.getOrganizacija());
		organizacija.getKorisnici().add(korisnik.toKorisnik());
		cs.organizacije.getOrganizacije().replace(organizacija.getIme(), organizacija);
		cs.korisnici.getKorisnici().put(korisnik.getEmail(), korisnik.toKorisnik());
		cs.upisiOrganizacije();
		cs.upisiKorisnike();
		return Response.status(Response.Status.OK).entity("Uspesno registrovan korisnik!").build();

	}
	
	@PUT
	@RolesAllowed({"SUPER_ADMIN", "ADMIN"})
	@Path("/izmeni/{email}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response izmeniKorisnika(KorisnikDTO korisnikDTO, @PathParam("email") String email){
		if (korisnikDTO.getIme().length() == 0 || korisnikDTO.getPrezime().length() == 0 || korisnikDTO.getLozinka().length() == 0) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Morate popuniti sva polja").build();
		} else if (korisnikDTO.getIme() == null || korisnikDTO.getPrezime() == null
				 || korisnikDTO.getLozinka() == null || korisnikDTO.getUloga() == null) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Morate popuniti sva polja").build();
		}
		Korisnik noviKorisnik = korisnikDTO.toKorisnik(email, cs.korisnici.getKorisnici().get(email).getOrganizacija());
		cs.korisnici.getKorisnici().remove(noviKorisnik.getEmail());
		cs.korisnici.getKorisnici().put(noviKorisnik.getEmail(), noviKorisnik);
		cs.upisiKorisnike();
		return Response.status(Response.Status.OK).entity("Uspesno izmenjen korisnik").build();
	}
	
	@GET
	@RolesAllowed({"SUPER_ADMIN", "ADMIN", "KORISNIK"})
	@Path("/get/{email}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getKorisnik(@PathParam("email") String email) {
		Korisnik korisnik = null;
		if(cs.korisnici.getKorisnici().containsKey(email)) {
			korisnik = cs.korisnici.getKorisnici().get(email);
			return Response.status(Response.Status.OK).entity(korisnik).build();
		
		}else {
			return Response.status(Response.Status.BAD_REQUEST).entity("Greska").build();
		
		}
	}
	
	@GET
	@RolesAllowed("ADMIN")
	@Path("/getOrganizacija")
	@Produces(MediaType.APPLICATION_JSON)
	public String getOrganizacijaAdmina() {
		Korisnik korisnik =(Korisnik) securityContext.getUserPrincipal();
		return cs.korisnici.getKorisnici().get(korisnik.getEmail()).getOrganizacija().getIme();
	}
	
	@DELETE
	@RolesAllowed({"SUPER_ADMIN", "ADMIN"})
	@Path("/obrisi/{email}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obrisi(@PathParam("email") String email) {
		Korisnik korisnikZaBrisanje = cs.korisnici.getKorisnici().get(email);
		Korisnik korisnikUlogovan =(Korisnik) securityContext.getUserPrincipal();
		if(email.equals(korisnikUlogovan.getEmail())) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Ne mozete obrisati svoj nalog!").build();
		}	
		Organizacija organizacijaKorisnika = cs.organizacije.getOrganizacije().get(korisnikZaBrisanje.getOrganizacija().getIme());
		for(Korisnik k: organizacijaKorisnika.getKorisnici()) {
			if(k.getEmail().equals(email)) {
				organizacijaKorisnika.getKorisnici().remove(k);
				break;
			}
		}
		cs.korisnici.getKorisnici().remove(email);
		cs.upisiOrganizacije();
		cs.upisiKorisnike();
		return Response.status(Response.Status.OK).entity("Uspesno obrisan korisnik!").build();
	}
	
	@PUT
	@RolesAllowed({"SUPER_ADMIN", "ADMIN", "KORISNIK"})
	@Path("/azuriraj/{email}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response azurirajProfil(AzurirajProfilDTO profilDTO, @PathParam("email") String email){
		if (profilDTO.getIme().length() == 0 || profilDTO.getPrezime().length() == 0 || profilDTO.getLozinka().length() == 0
				|| profilDTO.getLozinka1().length() == 0|| profilDTO.getEmail().length() == 0) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Morate popuniti sva polja!").build();
		} else if (profilDTO.getIme() == null || profilDTO.getPrezime() == null
				 || profilDTO.getLozinka() == null || profilDTO.getLozinka1() == null || profilDTO.getEmail() == null) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Morate popuniti sva polja!").build();
		}else if(!profilDTO.getLozinka().equals(profilDTO.getLozinka1())) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Lozinke se ne poklapaju!").build();
		}
		if (cs.korisnici.getKorisnici().containsKey(profilDTO.getEmail())&& (!profilDTO.getEmail().equalsIgnoreCase(email))) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Vec postoji korisnik sa unetim email-om!").build();
		}
		Korisnik noviKorisnik = profilDTO.toKorisnik(cs.korisnici.getKorisnici().get(email).getOrganizacija(),cs.korisnici.getKorisnici().get(email).getUloga());
		if(!noviKorisnik.getUloga().toString().equalsIgnoreCase("SUPER_ADMIN")) {
			Organizacija organizacija = noviKorisnik.getOrganizacija();
			for(Korisnik k: organizacija.getKorisnici()) {
				if(k.getEmail().equals(email)) {
					organizacija.getKorisnici().remove(k);
					break;
				}
			}
			organizacija.getKorisnici().add(noviKorisnik);
			cs.upisiOrganizacije();
		}
		cs.korisnici.getKorisnici().remove(email);
		cs.korisnici.getKorisnici().put(noviKorisnik.getEmail(), noviKorisnik);
		cs.upisiKorisnike();
		return Response.status(Response.Status.OK).entity("Uspesno azuriran profil!").build();
	}
	
	
}
