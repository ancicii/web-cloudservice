package dto;

public class KategroijeMaxDTO {
	private int brojJezgara;
	private int ram;
	private int gpu;
	public int getBrojJezgara() {
		return brojJezgara;
	}
	public void setBrojJezgara(int brojJezgara) {
		this.brojJezgara = brojJezgara;
	}
	public int getRam() {
		return ram;
	}
	public void setRam(int ram) {
		this.ram = ram;
	}
	public int getGpu() {
		return gpu;
	}
	public void setGpu(int gpu) {
		this.gpu = gpu;
	}
	public KategroijeMaxDTO(int brojJezgara, int ram, int gpu) {
		super();
		this.brojJezgara = brojJezgara;
		this.ram = ram;
		this.gpu = gpu;
	}
	public KategroijeMaxDTO() {
		super();
	}
	
	
	

}
