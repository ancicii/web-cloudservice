package beans;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import json.KorisnikJSON;


public class Korisnici {
	
	private HashMap<String, Korisnik> korisnici = new HashMap<String, Korisnik>();

	public HashMap<String, Korisnik> getKorisnici() {
		return korisnici;
	}

	public void setKorisnici(HashMap<String, Korisnik> korisnici) {
		this.korisnici = korisnici;
	}

	public Korisnici() {
		super();
		String line;
		String data = "";
		Gson gson = new Gson();
		HashMap<String, KorisnikJSON> korisniciJSON = new HashMap<String, KorisnikJSON>();
		try {
			File file = new File("D:\\Ana\\Web Projekat\\web-cloudservice\\CloudService\\WebContent\\resources\\korisnici.txt");
			BufferedReader br = new BufferedReader(new FileReader(file));
			try {
				while ((line = br.readLine())!=null){
					data += line;
				}
				korisniciJSON = gson.fromJson(data, new TypeToken<HashMap<String, KorisnikJSON>>(){}.getType()); 
				for(KorisnikJSON korisnikJSON: korisniciJSON.values()){
					korisnici.put(korisnikJSON.getEmail(), korisnikJSON.toKorisnik());
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
			 br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	

}
