package beans;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import json.DiskJSON;

public class Diskovi {
	
	private HashMap<String, Disk> diskovi = new HashMap<String, Disk>();

	public HashMap<String, Disk> getDiskovi() {
		return diskovi;
	}

	public void setDiskovi(HashMap<String, Disk> diskovi) {
		this.diskovi = diskovi;
	}

	public Diskovi() {
		super();
		
		String line;
		String data = "";
		Gson gson = new Gson();
		HashMap<String, DiskJSON> diskoviJSON = new HashMap<String, DiskJSON>();
		try {
			File file = new File("D:\\Ana\\Web Projekat\\web-cloudservice\\CloudService\\WebContent\\resources\\diskovi.txt");
			BufferedReader br = new BufferedReader(new FileReader(file));
			try {
				while ((line = br.readLine())!=null){
					data += line;
				}
				diskoviJSON = gson.fromJson(data, new TypeToken<HashMap<String, DiskJSON>>(){}.getType()); 
				for(DiskJSON diskJSON: diskoviJSON.values()){
					diskovi.put(diskJSON.getIme(), diskJSON.toDisk());
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
			 br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	

}
