package authentication;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.internal.util.Base64;

import beans.Korisnici;
import beans.Korisnik;
import beans.Uloga;

@Provider
public class AuthenticationFilter implements javax.ws.rs.container.ContainerRequestFilter {

	@Context
	private ResourceInfo resourceInfo;
	
	@Context
    SecurityContext securityContext;

	private static final String AUTHORIZATION_PROPERTY = "Authorization";
	private static final String AUTHENTICATION_SCHEME = "Basic";

	@Override
	public void filter(ContainerRequestContext requestContext) {
		Method method = resourceInfo.getResourceMethod();

		/*
		 * if (requestContext.getMethod().equalsIgnoreCase("OPTIONS") || method == null
		 * || method.isAnnotationPresent(PermitAll.class)) { return; }
		 */

		// Access allowed for all
		if (!method.isAnnotationPresent(PermitAll.class)) {
			// Access denied for all
			if (method.isAnnotationPresent(DenyAll.class)) {
				requestContext.abortWith(
						Response.status(Response.Status.FORBIDDEN).entity("Access blocked for all users !!").build());
				return;
			}

			// Get request headers
			final MultivaluedMap<String, String> headers = requestContext.getHeaders();

			// Fetch authorization header
			final List<String> authorization = headers.get(AUTHORIZATION_PROPERTY);

			// If no authorization information present; block access
			if (authorization == null || authorization.isEmpty()) {
				requestContext.abortWith(Response.status(Response.Status.FORBIDDEN)
						.entity("You cannot access this resource").build());
				return;
			}

			// Get encoded username and password
			final String encodedUserPassword = authorization.get(0).replaceFirst(AUTHENTICATION_SCHEME + " ", "");

			// Decode username and password
			String usernameAndPassword = new String(Base64.decode(encodedUserPassword.getBytes()));
			;

			// Split username and password tokens
			final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
			final String username = tokenizer.nextToken();
			final String password = tokenizer.nextToken();

			// Verify user access
			if (method.isAnnotationPresent(RolesAllowed.class)) {
				RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
				Set<String> rolesSet = new HashSet<String>(Arrays.asList(rolesAnnotation.value()));

				// Is user valid?
				if (!isUserAllowed(username, password, rolesSet, requestContext)) {
					requestContext.abortWith(Response.status(Response.Status.FORBIDDEN)
							.entity("You cannot access this resource").build());
					return;
				}
				
			}
		}
	}

	private boolean isUserAllowed(final String username, final String password, final Set<String> rolesSet, ContainerRequestContext requestContext) {
		boolean isAllowed = false;;
		Korisnik ulogovan = null;
		Korisnici korisnici = new Korisnici();
		if (korisnici.getKorisnici().containsKey(username)) {
			ulogovan = korisnici.getKorisnici().get(username);
			if (password.equals(ulogovan.getLozinka())) {
				String userRole = ulogovan.getUloga().toString();

				// Step 2. Verify user role
				if (rolesSet.contains(userRole)) {
					String scheme = requestContext.getUriInfo().getRequestUri().getScheme();
					requestContext.setSecurityContext(new MyApplicationSecurityContext(ulogovan, scheme));
					isAllowed = true;
				}
			}
		}
		return isAllowed;
	}
}
