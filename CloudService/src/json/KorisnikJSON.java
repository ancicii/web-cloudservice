package json;

import beans.Korisnik;
import beans.Uloga;
import exceptions.NotUniqueException;

public class KorisnikJSON {
	
	private String email;
	private String lozinka;
	private String ime;
	private String prezime;
	private String uloga;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLozinka() {
		return lozinka;
	}
	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getUloga() {
		return uloga;
	}
	public void setUloga(String uloga) {
		this.uloga = uloga;
	}
	public KorisnikJSON(String email, String lozinka, String ime, String prezime, String uloga) {
		super();
		this.email = email;
		this.lozinka = lozinka;
		this.ime = ime;
		this.prezime = prezime;
		this.uloga = uloga;
	}
	public KorisnikJSON() {
		super();
	}
	
	public KorisnikJSON(Korisnik noviKorisnik) {
		super();
		this.email = noviKorisnik.getEmail();
		this.lozinka = noviKorisnik.getLozinka();
		this.ime = noviKorisnik.getIme();
		this.prezime = noviKorisnik.getPrezime();
		this.uloga = noviKorisnik.getUloga().toString();
	}
	
	public Korisnik toKorisnik() throws NotUniqueException{
		Korisnik korisnik = new Korisnik(this.email,this.lozinka,this.ime,this.prezime,
				null,Uloga.valueOf(this.uloga));
		return korisnik;
	}

}
