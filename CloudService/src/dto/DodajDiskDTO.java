package dto;

public class DodajDiskDTO {
	
	private String ime;
	private String tip;
	private int kapacitet;
	private String virtualnaMasina;
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getTip() {
		return tip;
	}
	public void setTip(String tip) {
		this.tip = tip;
	}
	public int getKapacitet() {
		return kapacitet;
	}
	public void setKapacitet(int kapacitet) {
		this.kapacitet = kapacitet;
	}
	public String getVirtualnaMasina() {
		return virtualnaMasina;
	}
	public void setVirtualnaMasina(String virtualnaMasina) {
		this.virtualnaMasina = virtualnaMasina;
	}
	public DodajDiskDTO(String ime, String tip, int kapacitet, String virtualnaMasina) {
		super();
		this.ime = ime;
		this.tip = tip;
		this.kapacitet = kapacitet;
		this.virtualnaMasina = virtualnaMasina;
	}
	public DodajDiskDTO() {
		super();
	}
	
	

}
