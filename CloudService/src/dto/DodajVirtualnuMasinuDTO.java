package dto;

import java.util.ArrayList;

import beans.Disk;
import beans.Kategorija;
import beans.VirtualnaMasina;

public class DodajVirtualnuMasinuDTO {

	private String ime;
	private Kategorija kategorija;
	private ArrayList<Disk> diskovi;
	private String organizacija;
	public DodajVirtualnuMasinuDTO(String ime, Kategorija kategorija, ArrayList<Disk> diskovi,
			String organizacija) {
		super();
		this.ime = ime;
		this.kategorija = kategorija;
		this.diskovi = diskovi;
		this.organizacija = organizacija;
	}
	public DodajVirtualnuMasinuDTO() {
		super();
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public Kategorija getKategorija() {
		return kategorija;
	}
	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}
	public ArrayList<Disk> getDiskovi() {
		return diskovi;
	}
	public void setDiskovi(ArrayList<Disk> diskovi) {
		this.diskovi = diskovi;
	}
	public String getOrganizacija() {
		return organizacija;
	}
	public void setOrganizacija(String organizacija) {
		this.organizacija = organizacija;
	}
	public VirtualnaMasina toVirtualnaMasina() {
		return new VirtualnaMasina(this.ime, this.kategorija, this.diskovi);
	}
	
	
	
}
