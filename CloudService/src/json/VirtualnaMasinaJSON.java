package json;

import java.util.ArrayList;

import beans.Aktivnost;
import beans.Disk;
import beans.Kategorija;
import beans.VirtualnaMasina;

public class VirtualnaMasinaJSON {
	private String ime;
	private Kategorija kategorija;
	private ArrayList<DiskJSON> diskovi;
	private ArrayList<Aktivnost> aktivnosti;
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public Kategorija getKategorija() {
		return kategorija;
	}
	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}
	public ArrayList<DiskJSON> getDiskovi() {
		return diskovi;
	}
	public void setDiskovi(ArrayList<DiskJSON> diskovi) {
		this.diskovi = diskovi;
	}
	public ArrayList<Aktivnost> getAktivnosti() {
		return aktivnosti;
	}
	public void setAktivnosti(ArrayList<Aktivnost> aktivnosti) {
		this.aktivnosti = aktivnosti;
	}
	public VirtualnaMasinaJSON(String ime, Kategorija kategorija, ArrayList<DiskJSON> diskovi,
			ArrayList<Aktivnost> aktivnosti) {
		super();
		this.ime = ime;
		this.kategorija = kategorija;
		this.diskovi = diskovi;
		this.aktivnosti = aktivnosti;
	}
	public VirtualnaMasinaJSON() {
		super();
	}
	public VirtualnaMasinaJSON(VirtualnaMasina vm) {
		super();
		this.ime = vm.getIme();
		this.kategorija = vm.getKategorija();
		this.aktivnosti = vm.getAktivnosti();
		ArrayList<DiskJSON> diskoviJSON = new ArrayList<DiskJSON>();
		for(Disk disk: vm.getDiskovi()) {
			diskoviJSON.add(new DiskJSON(disk.getIme(), disk.getTip().toString(), disk.getKapacitet()));
			
		}
		this.diskovi = diskoviJSON;
	}
	
	public VirtualnaMasina toVirtualnaMasina(){
		ArrayList<Disk> diskoviVM = new ArrayList<>();
		for(DiskJSON diskJSON: this.diskovi){
			diskoviVM.add(diskJSON.toDisk());
		}
		VirtualnaMasina vm = new VirtualnaMasina(this.ime, this.kategorija, diskoviVM, this.aktivnosti);
		return vm;
	}
	
	

}
