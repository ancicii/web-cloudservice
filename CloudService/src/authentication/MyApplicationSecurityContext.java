package authentication;
 
import javax.ws.rs.core.SecurityContext;
import beans.Korisnik;
import java.security.Principal;
 
/**
 * Custom Security Context.
 * 
 * @author Deisss (MIT License)
*/
public class MyApplicationSecurityContext implements SecurityContext {
    private Korisnik user;
    private String scheme;
 
    public MyApplicationSecurityContext(Korisnik user, String scheme) {
        this.user = user;
        this.scheme = scheme;
    }
 
    @Override
    public Principal getUserPrincipal() {return this.user;}
 
    @Override
    public boolean isUserInRole(String s) {
        if (user.getUloga().toString() != null) {
            return user.getUloga().toString().equalsIgnoreCase(s);
        }
        return false;
    }
 
    @Override
    public boolean isSecure() {return "https".equals(this.scheme);}
 
    @Override
    public String getAuthenticationScheme() {
        return SecurityContext.BASIC_AUTH;
    }
}