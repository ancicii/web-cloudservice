package dto;

import java.util.ArrayList;

import beans.Aktivnost;
import beans.Disk;
import beans.Kategorija;
import beans.VirtualnaMasina;

public class VirtualnaMasinaIzmenaDTO {
	private String ime;
	private Kategorija kategorija;
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public Kategorija getKategorija() {
		return kategorija;
	}
	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}
	public VirtualnaMasinaIzmenaDTO(String ime, Kategorija kategorija) {
		super();
		this.ime = ime;
		this.kategorija = kategorija;
	}
	public VirtualnaMasinaIzmenaDTO() {
		super();
	}
	
	public VirtualnaMasina toVirtualnaMasina(ArrayList<Disk> diskovi, ArrayList<Aktivnost> aktivnosti) {
		VirtualnaMasina virtualnaMasina = new VirtualnaMasina(this.ime, this.kategorija, diskovi, aktivnosti);
		return virtualnaMasina;
	}
	

}
