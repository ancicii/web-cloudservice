package dto;

import java.util.ArrayList;

import beans.Aktivnost;
import beans.Disk;
import beans.Kategorija;
import beans.Organizacija;
import beans.VirtualnaMasina;

public class VirtualnaMasinaGetDTO {

	private String ime;
	private Kategorija kategorija;
	private ArrayList<Disk> diskovi;
	private ArrayList<AktivnostDTO> aktivnosti;
	private Organizacija organizacija;
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public Kategorija getKategorija() {
		return kategorija;
	}
	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}
	public ArrayList<Disk> getDiskovi() {
		return diskovi;
	}
	public void setDiskovi(ArrayList<Disk> diskovi) {
		this.diskovi = diskovi;
	}
	public ArrayList<AktivnostDTO> getAktivnosti() {
		return aktivnosti;
	}
	public void setAktivnosti(ArrayList<AktivnostDTO> aktivnosti) {
		this.aktivnosti = aktivnosti;
	}
	public Organizacija getOrganizacija() {
		return organizacija;
	}
	public void setOrganizacija(Organizacija organizacija) {
		this.organizacija = organizacija;
	}
	public VirtualnaMasinaGetDTO(String ime, Kategorija kategorija, ArrayList<Disk> diskovi,
			ArrayList<AktivnostDTO> aktivnosti, Organizacija organizacija) {
		super();
		this.ime = ime;
		this.kategorija = kategorija;
		this.diskovi = diskovi;
		this.aktivnosti = aktivnosti;
		this.organizacija = organizacija;
	}
	public VirtualnaMasinaGetDTO() {
		super();
	}
	public VirtualnaMasinaGetDTO(VirtualnaMasina virtualnaMasina, Organizacija organizacija) {
		super();
		this.ime = virtualnaMasina.getIme();
		this.kategorija = virtualnaMasina.getKategorija();
		this.diskovi = virtualnaMasina.getDiskovi();
		ArrayList<AktivnostDTO> aktivnostiDTO = new ArrayList<AktivnostDTO>();
		for(Aktivnost a: virtualnaMasina.getAktivnosti()) {
			aktivnostiDTO.add(new AktivnostDTO(a));
		}
		this.aktivnosti = aktivnostiDTO;
		this.organizacija = organizacija;
	}
	
	
}
