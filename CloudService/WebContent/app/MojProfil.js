Vue.component("moj-profil", {
	data: function () {
		return {
			korisnik: {
				email: "",
				ime: "",
				prezime: "",
				lozinka: "",
				lozinka2: "",
				organizacija: "",
				uloga: ""
			},
			email: sessionStorage.getItem("korisnik"),
			uloga: sessionStorage.getItem("tip")
		}
	},
	template: `
	<div class="organizacije1">
	<div class="container cntBg text-dark pt-5 pb-5 mb-2 bootstrap snippet">
	<div class="row">
		<div class="col-lg-12">
    <button disabled class="nav-link btn btn-custom float-right" title="Obrisi korisnika"><i class="fa fa-user-times fa-2x" style="color:#6C1539;"></i></button>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<form class="form" action="##" method="post" id="organizacijaForm">
			<div class="form-group">
				<div class="col-xs-6">
					<label for="email"><h4 class="font">Email</h4></label>
					<input type="text" v-model="korisnik.email" class="form-control" name="email" id="email" title="Unesite email korisnika">
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-6">
					<label for="ime"><h4 class="font">Ime</h4></label>
					<input type="text" v-model="korisnik.ime" class="form-control" name="ime" id="ime" title="Unesite ime korisnika">
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-6">
					<label for="prezime"><h4 class="font">Prezime</h4></label>
					<input type="text" v-model="korisnik.prezime" class="form-control" name="prezime" id="prezime" title="Unesite prezime korisnika">
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-6">
					<label for="lozinka"><h4 class="font">Lozinka</h4></label>
					<input type="password" v-model="korisnik.lozinka" class="form-control" name="lozinka" id="lozinka" title="Unesite sifru korisnika">
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-6">
					<label for="lozinka1"><h4 class="font">Ponovite lozinku</h4></label>
					<input type="password" v-model="korisnik.lozinka1" class="form-control" name="lozinka1" id="lozinka1" title="Unesite sifru korisnika">
				</div>
			</div>
			<div class="form-group" v-if="uloga!='superAdmin'">
				<div class="col-xs-6">
				<label for="organizacija"><h4 class="font">Organizacija</h4></label>					  
					<select disabled style="font-family: Montserrat; font-size:14px" id="organizacija" class="form-control" v-model="korisnik.organizacija">
						<option selected>{{korisnik.organizacija}}</option>
					</select>  
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-6">
				<label for="uloga"><h4 class="font">Tip korisnika</h4></label>
					<select disabled style="font-family: Montserrat; font-size:14px" id="uloga" class="form-control" v-model="korisnik.uloga">
						<option selected>{{korisnik.uloga}}</option>
					</select> 
				</div>
			</div>
			<div class="form-group">
			<div class="row">
				<div class="col">
				<input type="button" class="orgBtn" value="Sacuvaj" @click="azurirajProfil">
				</div>
				<div class="col">
				<input type="button" class="orgBtn" @click = "nazad" value="Nazad">
				</div>
				</div>
			</div>
			
			</form>
			
		</div><!--/col-9-->
		
	</div><!--/row-->
	
</div>
</div>
	
	`,
	methods: {
		nazad: function () {
			router.go(-1);
		},
		nadjiKorisnika: function () {
			axios
				.get('/korisnici/get/' + this.email, { headers: { Authorization: sessionStorage.getItem("Authorization") } })
				.then(response => {
					if (response.status === 200) {
						this.korisnik.email = response.data.email;
						this.korisnik.ime = response.data.ime;
						this.korisnik.prezime = response.data.prezime;
						this.korisnik.lozinka = response.data.lozinka;
						this.korisnik.lozinka1 = response.data.lozinka;
						if (this.uloga != "superAdmin") {
							this.korisnik.organizacija = response.data.organizacija.ime;
						}
						this.korisnik.uloga = response.data.uloga;
					}

				})
				.catch(error => { alert(error.response.data) });
		},
		azurirajProfil: function () {
			let kor = {
				ime: this.korisnik.ime,
				prezime: this.korisnik.prezime,
				lozinka: this.korisnik.lozinka,
				lozinka1: this.korisnik.lozinka1,
				email: this.korisnik.email

			};
			axios
				.put('/korisnici/azuriraj/' + this.email, kor, { headers: { Authorization: sessionStorage.getItem("Authorization") } })
				.then(response => {
					if (response.status === 200) {
						this.login(kor);
						router.go(-1);
					};
				}).catch(error => { alert(error.response.data) });

		},
		 login: function(kor){
            const koris = {
                email: kor.email,
                lozinka: kor.lozinka
				};
            sessionStorage.setItem("Authorization", btoa(koris.email + ":" + koris.lozinka));
            axios
            .post('/cloudy/login',koris,{ headers: { Authorization: sessionStorage.getItem("Authorization") } })
			.then(response => (this.init(response, koris)))
			.catch(error => (alert(error.response.data)));		
		},
        init: function(response, koris){
			if(response.data === "ADMIN"){
                sessionStorage.setItem("tip", "admin");
                sessionStorage.setItem("korisnik", koris.email);                
            }else if(response.data === "KORISNIK"){
                sessionStorage.setItem("tip", "korisnik")
                sessionStorage.setItem("korisnik", koris.email);
            }else{
                sessionStorage.setItem("tip", "superAdmin");
                sessionStorage.setItem("korisnik", koris.email);
            }
				
		}
	},
	mounted() {
		this.nadjiKorisnika();
	}


});