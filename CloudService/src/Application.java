import org.glassfish.jersey.server.ResourceConfig;

import authentication.AuthenticationFilter;
import authentication.GsonMessageBodyHandler;

public class Application extends ResourceConfig 
{
	public Application() 
	{
		packages("services");
		register(GsonMessageBodyHandler.class);
		register(AuthenticationFilter.class);
	}
}