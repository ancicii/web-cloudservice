package services;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import beans.Aktivnost;
import beans.Disk;
import beans.Korisnik;
import beans.Organizacija;
import beans.VirtualnaMasina;
import dto.AktivnostiIzmenaDTO;
import dto.DodajVirtualnuMasinuDTO;
import dto.PretragaDTO;
import dto.VirtualnaMasinaGetDTO;
import dto.VirtualnaMasinaIzmenaDTO;
import dto.VirtualneMasineDTO;

@DeclareRoles({ "SUPER_ADMIN", "ADMIN", "KORISNIK" })
@Path("/virtualneMasine")
public class ServiceVirtualneMasine {

	@Context
	SecurityContext securityContext;

	CloudService cs = new CloudService();

	@GET
	@RolesAllowed({ "SUPER_ADMIN", "ADMIN", "KORISNIK" })
	@Path("/pregled")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<VirtualneMasineDTO> getVirtualneMasine() {
		Collection<VirtualneMasineDTO> list = new ArrayList<VirtualneMasineDTO>();
		Korisnik korisnik = (Korisnik) securityContext.getUserPrincipal();

		if (korisnik.getUloga().toString().equalsIgnoreCase("SUPER_ADMIN")) {
			for (Organizacija o : cs.organizacije.getOrganizacije().values()) {
				for (Object resurs : o.getResursi()) {
					if (resurs instanceof VirtualnaMasina) {
						VirtualnaMasina vm = (VirtualnaMasina) resurs;
						list.add(new VirtualneMasineDTO(vm.getIme(), vm.getKategorija(), o));
					}
				}
			}
		} else {
			for (Organizacija o : cs.organizacije.getOrganizacije().values()) {
				if (cs.korisnici.getKorisnici().get(korisnik.getEmail()).getOrganizacija().getIme()
						.equals(o.getIme())) {
					for (Object resurs : o.getResursi()) {
						if (resurs instanceof VirtualnaMasina) {
							VirtualnaMasina vm = (VirtualnaMasina) resurs;
							list.add(new VirtualneMasineDTO(vm.getIme(), vm.getKategorija(), o));
						}
					}
				}
			}
		}
		return list;
	}

	@POST
	@RolesAllowed({ "SUPER_ADMIN", "ADMIN" })
	@Path("/dodaj")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response dodajVirtualnuMasinu(DodajVirtualnuMasinuDTO vmDTO) {
		if (vmDTO.getIme().length() == 0) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Morate popuniti sva polja").build();
		} else if (vmDTO.getIme() == null || vmDTO.getKategorija() == null || vmDTO.getDiskovi() == null
				|| vmDTO.getOrganizacija() == null) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Morate popuniti sva polja").build();
		}
		if (cs.virtualneMasine.getVirtualneMasine().containsKey(vmDTO.getIme())) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Vec postoji virtualna masina sa unetim imenom!")
					.build();
		}
		if (!cs.organizacije.getOrganizacije().containsKey(vmDTO.getOrganizacija())) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Organizacija sa izabranim imenom ne postoji!")
					.build();
		}
		if (!cs.kategorije.getKategorije().containsKey(vmDTO.getKategorija().getIme())) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Kategorija sa izabranim imenom ne postoji!")
					.build();
		}
		for (Disk d : vmDTO.getDiskovi()) {
			if (!cs.diskovi.getDiskovi().containsKey(d.getIme())) {
				return Response.status(Response.Status.BAD_REQUEST).entity("Disk sa izabranim imenom ne postoji!")
						.build();
			} else if (d.getVirtualnaMasina() != null) {
				return Response.status(Response.Status.BAD_REQUEST)
						.entity("Disk je vec zakacen na jednu od virtualnih masina!").build();
			}
		}
		Organizacija organizacija = cs.organizacije.getOrganizacije().get(vmDTO.getOrganizacija());
		organizacija.getResursi().add(vmDTO.toVirtualnaMasina());

		for (Disk d : vmDTO.getDiskovi()) {
			Disk d1 = cs.diskovi.getDiskovi().get(d.getIme());
			d1.setVirtualnaMasina(vmDTO.toVirtualnaMasina());
			cs.diskovi.getDiskovi().replace(d1.getIme(), d1);
			organizacija.getResursi().add(d1);
		}
		cs.organizacije.getOrganizacije().replace(organizacija.getIme(), organizacija);
		cs.upisiOrganizacije();
		cs.virtualneMasine.getVirtualneMasine().put(vmDTO.getIme(), vmDTO.toVirtualnaMasina());
		cs.upisiVirtualneMasine();
		return Response.status(Response.Status.OK).entity("Uspesno dodata virtualna masina!").build();

	}

	@GET
	@RolesAllowed({ "SUPER_ADMIN", "ADMIN", "KORISNIK" })
	@Path("/get/{naziv}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getVirtualnaMasina(@PathParam("naziv") String naziv) {
		VirtualnaMasinaGetDTO virtualnaMasinaDTO = null;
		Organizacija organizacija = null;
		VirtualnaMasina virtualnaMasina = null;
		for (Organizacija o : cs.organizacije.getOrganizacije().values()) {
			for (Object resurs : o.getResursi()) {
				if (resurs instanceof VirtualnaMasina) {
					VirtualnaMasina vm = (VirtualnaMasina) resurs;
					if (vm.getIme().equals(naziv)) {
						virtualnaMasina = vm;
						organizacija = o;
						break;
					}
				}
			}
		}
		if (cs.virtualneMasine.getVirtualneMasine().containsKey(naziv)) {
			virtualnaMasinaDTO = new VirtualnaMasinaGetDTO(virtualnaMasina, organizacija);
			return Response.status(Response.Status.OK).entity(virtualnaMasinaDTO).build();
		} else {
			return Response.status(Response.Status.BAD_REQUEST).entity("Greska").build();
		}
	}

	@POST
	@RolesAllowed({ "SUPER_ADMIN", "ADMIN" })
	@Path("/izmeniAktivnosti/{naziv}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response izmeniAktivnosti(@PathParam("naziv") String naziv, ArrayList<AktivnostiIzmenaDTO> aktivnosti) {

		ArrayList<Aktivnost> aktivnostiList = new ArrayList<Aktivnost>();

		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
		for (AktivnostiIzmenaDTO ak : aktivnosti) {
			Aktivnost aktivnost = new Aktivnost();
			OffsetDateTime dateUgasenaOffset = null;
			OffsetDateTime dateUpaljenaOffset = null;
			if (ak.getDatumUgasena().equals("")) {
				dateUgasenaOffset = null;
			} else if (ak.getDatumUgasena() != null) {
				String dateUgasena = ak.getDatumUgasena() + ":30+01:00";
				dateUgasenaOffset = OffsetDateTime.parse(dateUgasena, dateTimeFormatter);

			}
			if (ak.getDatumUpaljena().equals("")) {
				dateUpaljenaOffset = null;
			} else if (ak.getDatumUpaljena() != null) {
				String dateUpaljena = ak.getDatumUpaljena() + ":30+01:00";
				dateUpaljenaOffset = OffsetDateTime.parse(dateUpaljena, dateTimeFormatter);
			}
			if (dateUgasenaOffset != null && dateUpaljenaOffset != null) {
				if (dateUgasenaOffset.isBefore(dateUpaljenaOffset)) {
					return Response.status(Response.Status.BAD_REQUEST)
							.entity("Krajnji datum aktivnosti ne moze biti pre pocetnog!").build();
				}
			} else if (dateUgasenaOffset != null && dateUpaljenaOffset == null) {
				return Response.status(Response.Status.BAD_REQUEST).entity("Morate postaviti pocetak aktivnosti!")
						.build();
			} else if (dateUgasenaOffset == null && dateUpaljenaOffset == null) {
				continue;
			}

			aktivnost.setDatumUgasena(dateUgasenaOffset);
			aktivnost.setDatumUpaljena(dateUpaljenaOffset);
			aktivnostiList.add(aktivnost);
		}
		VirtualnaMasina vm = cs.virtualneMasine.getVirtualneMasine().get(naziv);
		vm.setAktivnosti(aktivnostiList);
		cs.virtualneMasine.getVirtualneMasine().replace(naziv, vm);
		cs.upisiVirtualneMasine();
		return Response.status(Response.Status.OK).entity("Aktivnosti uspesno izmenjene!").build();

	}

	@PUT
	@RolesAllowed({ "SUPER_ADMIN", "ADMIN" })
	@Path("/izmeni/{ime}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response izmeniVirtualnuMasinu(VirtualnaMasinaIzmenaDTO vmDTO, @PathParam("ime") String ime) {
		if (vmDTO.getIme().length() == 0) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Morate popuniti sva polja!").build();
		} else if (vmDTO.getIme() == null || vmDTO.getKategorija() == null) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Morate popuniti sva polja!").build();
		}
		if (cs.virtualneMasine.getVirtualneMasine().containsKey(vmDTO.getIme())
				&& (!vmDTO.getIme().equalsIgnoreCase(ime))) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Vec postoji virtualna masina sa unetim imenom!")
					.build();
		}
		ArrayList<Disk> diskovi = cs.virtualneMasine.getVirtualneMasine().get(ime).getDiskovi();
		ArrayList<Aktivnost> aktivnosti = cs.virtualneMasine.getVirtualneMasine().get(ime).getAktivnosti();
		VirtualnaMasina novaVM = vmDTO.toVirtualnaMasina(diskovi, aktivnosti);

		cs.virtualneMasine.getVirtualneMasine().remove(ime);
		cs.virtualneMasine.getVirtualneMasine().put(novaVM.getIme(), novaVM);

		for (Organizacija o : cs.organizacije.getOrganizacije().values()) {
			for (Object resurs : o.getResursi()) {
				if (resurs instanceof VirtualnaMasina) {
					if (((VirtualnaMasina) resurs).getIme().equals(ime)) {
						o.getResursi().remove(resurs);
						o.getResursi().add(novaVM);
						break;
					}
				}
			}
		}

		cs.upisiOrganizacije();
		cs.upisiVirtualneMasine();
		return Response.status(Response.Status.OK).entity("Uspesno izmenjena virtualna masina!").build();
	}

	@DELETE
	@RolesAllowed({ "SUPER_ADMIN", "ADMIN" })
	@Path("/obrisi/{imeVM}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obrisi(@PathParam("imeVM") String imeVM) {
		VirtualnaMasina vmZaBrisanje = cs.virtualneMasine.getVirtualneMasine().get(imeVM);
		for (Disk disk : vmZaBrisanje.getDiskovi()) {
			cs.diskovi.getDiskovi().get(disk.getIme()).setVirtualnaMasina(null);
		}
		ArrayList<Object> found = new ArrayList<Object>();
		for (Organizacija organizacija : cs.organizacije.getOrganizacije().values()) {
			for (Object o : organizacija.getResursi()) {
				if (o instanceof VirtualnaMasina) {
					VirtualnaMasina vm = (VirtualnaMasina) o;
					if (vm.getIme().equals(imeVM)) {
						found.add(o);
					}
				} else {
					Disk disk = (Disk) o;
					for (Disk d : vmZaBrisanje.getDiskovi()) {
						if (d.getIme().equals(disk.getIme())) {
							found.add(o);
						}
					}
				}
			}
			organizacija.getResursi().removeAll(found);
		}
		cs.upisiOrganizacije();

		cs.virtualneMasine.getVirtualneMasine().remove(imeVM);
		cs.upisiVirtualneMasine();
		return Response.status(Response.Status.OK).entity("Uspesno obrisana virtualna masina!").build();
	}

	@PUT
	@RolesAllowed({ "SUPER_ADMIN", "ADMIN", "KORISNIK" })
	@Path("/pretrazi")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response pretrazi(PretragaDTO params) {
		Collection<VirtualneMasineDTO> list = new ArrayList<VirtualneMasineDTO>();
		Korisnik korisnik = (Korisnik) securityContext.getUserPrincipal();
		Collection<VirtualneMasineDTO> pretragaVM = new ArrayList<VirtualneMasineDTO>();
		Collection<VirtualneMasineDTO> pomocna = new ArrayList<VirtualneMasineDTO>();

		if (korisnik.getUloga().toString().equalsIgnoreCase("SUPER_ADMIN")) {
			for (Organizacija o : cs.organizacije.getOrganizacije().values()) {
				for (Object resurs : o.getResursi()) {
					if (resurs instanceof VirtualnaMasina) {
						VirtualnaMasina vm = (VirtualnaMasina) resurs;
						list.add(new VirtualneMasineDTO(vm.getIme(), vm.getKategorija(), o));
					}
				}
			}
		} else {
			for (Organizacija o : cs.organizacije.getOrganizacije().values()) {
				if (cs.korisnici.getKorisnici().get(korisnik.getEmail()).getOrganizacija().getIme()
						.equals(o.getIme())) {
					for (Object resurs : o.getResursi()) {
						if (resurs instanceof VirtualnaMasina) {
							VirtualnaMasina vm = (VirtualnaMasina) resurs;
							list.add(new VirtualneMasineDTO(vm.getIme(), vm.getKategorija(), o));
						}
					}
				}
			}
		}

		// prvo doda sve vm sa nazivom iz pretrage
		for (VirtualneMasineDTO vm : list) {
			if (params.getNazivVM().length() == 0) {
				pretragaVM.add(vm);
			} else if (vm.getIme().toUpperCase().contains(params.getNazivVM().toUpperCase())) {
				pretragaVM.add(vm);
			}
		}

		for (VirtualneMasineDTO vm : pretragaVM) {
			if(params.getJezgraDo()==0 && params.getJezgraOd()==0) {
				pomocna.add(vm);
			}
			else if (vm.getKategorija().getBrojJezgara() <= params.getJezgraDo()
					&& vm.getKategorija().getBrojJezgara() >= params.getJezgraOd()) {
				pomocna.add(vm);
			}
		}
		pretragaVM.clear();
		for (VirtualneMasineDTO vm : pomocna) {
			if(params.getRamDo()==0 && params.getRamOd()==0) {
				pretragaVM.add(vm);
			}
			else if (vm.getKategorija().getRAM() <= params.getRamDo() && vm.getKategorija().getRAM() >= params.getRamOd()) {
				pretragaVM.add(vm);
			}
		}
		pomocna.clear();
		for (VirtualneMasineDTO vm : pretragaVM) {
			if(params.getGpuDo()==0 && params.getGpuOd()==0) {
				pomocna.add(vm);
			}
			else if (vm.getKategorija().getGPUJezgra() <= params.getGpuDo()
					&& vm.getKategorija().getGPUJezgra() >= params.getGpuOd()) {
				pomocna.add(vm);	
			}
		}

		return Response.status(Response.Status.OK).entity(pomocna).build();

	}

}
