Vue.component("dodaj-izmeni-korisnika", {
	data: function(){
		return {
			korisnik: {
				email: "",
				ime: "",
				prezime: "",
				lozinka: "",
				organizacija: "",
				uloga: ""
			},
			izmeni: sessionStorage.getItem("izmeniK"),
			emailZaIzmenu: sessionStorage.getItem("emailKorisnika"),
			uloga: sessionStorage.getItem("tip"),
			organizacije: null,
			organizacijaAdmina: "",
		}
	},
	template: `
<div class="organizacije1">
    <div class="container cntBg text-dark pt-5 pb-5 mb-2 bootstrap snippet">
        <div class="row" v-if="izmeni=='da'">
            <div class="col-lg-12">
                <button @click="obrisi" class="nav-link btn btn-custom float-right" title="Obrisi korisnika"><i class="fa fa-user-times fa-2x" style="color:#6C1539;"></i></button>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <form class="form" action="##" method="post" id="organizacijaForm">
                    <div class="form-group">
                        <div class="col-xs-6">
                            <label for="email">
                                <h4 class="font">Email</h4></label>
                            <input type="text" v-if="izmeni=='da'" v-model="korisnik.email" class="form-control" name="email" id="email" placeholder="email korisnika" title="Unesite email korisnika" disabled>
                            <input type="text" v-if="izmeni=='ne'" v-model="korisnik.email" class="form-control" name="email" id="email" placeholder="email korisnika" title="Unesite email korisnika">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-6">
                            <label for="ime">
                                <h4 class="font">Ime</h4></label>
                            <input type="text" v-model="korisnik.ime" class="form-control" name="ime" id="ime" placeholder="ime korisnika" title="Unesite ime korisnika">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-6">
                            <label for="prezime">
                                <h4 class="font">Prezime</h4></label>
                            <input type="text" v-model="korisnik.prezime" class="form-control" name="prezime" id="prezime" placeholder="prezime korisnika" title="Unesite prezime korisnika">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-6">
                            <label for="lozinka">
                                <h4 class="font">lozinka</h4></label>
                            <input type="password" v-model="korisnik.lozinka" class="form-control" name="lozinka" id="lozinka" placeholder="lozinka korisnika" title="Unesite sifru korisnika">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-6">
                            <label for="organizacija">
                                <h4 class="font">Organizacija</h4></label>
                            <select v-if="izmeni=='ne'&& uloga=='superAdmin'" style="font-family: Montserrat; font-size:14px" id="organizacija" class="form-control" v-model="korisnik.organizacija">
                                <option value="" disabled selected>Izaberite organizaciju</option>
                                <option v-for="(org, index) in organizacije" :key="index" :value="org.ime">{{ org.ime }}</option>
                            </select>
                            <select v-if="izmeni=='ne' && uloga=='admin'" disabled style="font-family: Montserrat; font-size:14px" id="organizacija" class="form-control" v-model="organizacijaAdmina">
                                <option selected>{{organizacijaAdmina}}</option>
                            </select>
                            <select v-if="izmeni=='da'" disabled style="font-family: Montserrat; font-size:14px" id="organizacija" class="form-control" v-model="korisnik.organizacija">
                                <option selected>{{korisnik.organizacija}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-6">
                            <label for="uloga">
                                <h4 class="font">Tip korisnika</h4></label>
                            <select style="font-family: Montserrat; font-size:14px" id="uloga" class="form-control" v-model="korisnik.uloga">
                                <option value="" disabled selected>Izaberite tip korisnika</option>
                                <option value="ADMIN">Admin</option>
                                <option value="KORISNIK">Korisnik</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <input type="button" class="orgBtn" v-if="izmeni === 'ne'" value="Dodaj" @click="dodajKorisnika">
                                <input type="button" class="orgBtn" v-if="izmeni === 'da'" value="Izmeni" @click="izmeniKorisnika">
                            </div>
                            <div class="col">
                                <input type="button" class="orgBtn" @click="nazad" value="Nazad">
                            </div>
                        </div>
                    </div>

                </form>

            </div>
            <!--/col-9-->

        </div>
        <!--/row-->

    </div>
</div>
	`,
methods: { 
    nazad: function(){
    	router.push('/korisnici/pregled')
    },
    dodajKorisnika: function(){
    	let kor = {
    			email: this.korisnik.email,
    			ime: this.korisnik.ime,
    			prezime: this.korisnik.prezime,
    			lozinka: this.korisnik.lozinka,
					organizacija: this.korisnik.organizacija,
					uloga: this.korisnik.uloga
    	};
    	axios
    	.post('/korisnici/dodaj', kor, { headers: { Authorization: sessionStorage.getItem("Authorization") } })
    	.then(response =>{
    		if(response.status === 200){
    			router.push('/korisnici/pregled')
    		};
    	}).catch(error => {alert(error.response.data)});
    },
    nadjiKorisnika: function(){
    	axios
    	.get('/korisnici/get/'+this.emailZaIzmenu, { headers: { Authorization: sessionStorage.getItem("Authorization") } })
    	 .then(response=>{
				this.korisnik.email = response.data.email;
	    	this.korisnik.ime = response.data.ime;
	    	this.korisnik.prezime = response.data.prezime;
	    	this.korisnik.lozinka = response.data.lozinka;
				this.korisnik.organizacija = response.data.organizacija.ime;
				this.korisnik.uloga = response.data.uloga;
    	 })
    	.catch(error=>{alert(error.response.data)});
    },
    izmeniKorisnika: function(){
    	let kor = {
    			ime: this.korisnik.ime,
    			prezime: this.korisnik.prezime,
					lozinka: this.korisnik.lozinka,
					uloga: this.korisnik.uloga
    	};
    	axios
    	.put('/korisnici/izmeni/' + this.emailZaIzmenu, kor, { headers: { Authorization: sessionStorage.getItem("Authorization") } })
    	.then(response =>{
    		if(response.status === 200){
    			router.push('/korisnici/pregled')
    		};
    	}).catch(error => {alert(error.response.data)});
		},
		obrisi: function(){
			axios.delete('/korisnici/obrisi/' + this.emailZaIzmenu, { headers: { Authorization: sessionStorage.getItem("Authorization") } })
			.then(response=>{
				if(response.status === 200){
					router.push('/korisnici/pregled')
				};
			}).catch(error=>{alert(error.response.data)});
		}
	},	
  mounted(){
	  if(sessionStorage.getItem("tip")==="superAdmin"){
		  axios.
			get('/organizacije/pregled', { headers: { Authorization: sessionStorage.getItem("Authorization") } })
			.then(response => {
				this.organizacije = response.data;
			});  
		}else{
			axios
				.get('/korisnici/getOrganizacija', { headers: { Authorization: sessionStorage.getItem("Authorization") } })
				.then(response =>{
					if(response.status === 200){
						console.log(response.data);
						this.organizacijaAdmina = response.data;
						this.korisnik.organizacija = response.data;
					};
				}).catch(error => {alert(error.response.data)});
		}
	  if(this.izmeni==="da"){
		  this.nadjiKorisnika();
		}
	}


});