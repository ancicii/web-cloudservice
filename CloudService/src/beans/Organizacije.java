package beans;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import json.OrganizacijaJSON;

public class Organizacije {
	
	private HashMap<String, Organizacija> organizacije = new HashMap<String, Organizacija>();

	public HashMap<String, Organizacija> getOrganizacije() {
		return organizacije;
	}

	public void setOrganizacije(HashMap<String, Organizacija> organizacije) {
		this.organizacije = organizacije;
	}

	public Organizacije(Diskovi diskovi, VirtualneMasine virtualneMasine) {
		super();
		
		String line;
		String data = "";
		Gson gson = new Gson();
		HashMap<String, OrganizacijaJSON> organizacijeJSON = new HashMap<String, OrganizacijaJSON>();
		try {
			File file = new File("D:\\Ana\\Web Projekat\\web-cloudservice\\CloudService\\WebContent\\resources\\organizacije.txt");
			BufferedReader br = new BufferedReader(new FileReader(file));
			try {
				while ((line = br.readLine())!=null){
					data += line;
				}
				organizacijeJSON = gson.fromJson(data, new TypeToken<HashMap<String, OrganizacijaJSON>>(){}.getType()); 
				for(OrganizacijaJSON organizacijaJSON: organizacijeJSON.values()){
					Organizacija o = organizacijaJSON.toOrganizacija(diskovi, virtualneMasine);
					organizacije.put(organizacijaJSON.getIme(), o);
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
			 br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	

}
