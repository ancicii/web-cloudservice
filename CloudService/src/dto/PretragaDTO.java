package dto;

public class PretragaDTO {
	
	private int jezgraOd;
	private int jezgraDo;
	private int ramOd;
	private int ramDo;
	private int gpuOd;
	private int gpuDo;
	private String nazivVM;
	public int getJezgraOd() {
		return jezgraOd;
	}
	public void setJezgraOd(int jezgraOd) {
		this.jezgraOd = jezgraOd;
	}
	public int getJezgraDo() {
		return jezgraDo;
	}
	public void setJezgraDo(int jezgraDo) {
		this.jezgraDo = jezgraDo;
	}
	public int getRamOd() {
		return ramOd;
	}
	public void setRamOd(int ramOd) {
		this.ramOd = ramOd;
	}
	public int getRamDo() {
		return ramDo;
	}
	public void setRamDo(int ramDo) {
		this.ramDo = ramDo;
	}
	public int getGpuOd() {
		return gpuOd;
	}
	public void setGpuOd(int gpuOd) {
		this.gpuOd = gpuOd;
	}
	public int getGpuDo() {
		return gpuDo;
	}
	public void setGpuDo(int gpuDo) {
		this.gpuDo = gpuDo;
	}
	public String getNazivVM() {
		return nazivVM;
	}
	public void setNazivVM(String nazivVM) {
		this.nazivVM = nazivVM;
	}
	public PretragaDTO(int jezgraOd, int jezgraDo, int ramOd, int ramDo, int gpuOd, int gpuDo, String nazivVM) {
		super();
		this.jezgraOd = jezgraOd;
		this.jezgraDo = jezgraDo;
		this.ramOd = ramOd;
		this.ramDo = ramDo;
		this.gpuOd = gpuOd;
		this.gpuDo = gpuDo;
		this.nazivVM = nazivVM;
	}
	public PretragaDTO() {
		super();
	}


}
