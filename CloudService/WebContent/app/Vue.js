axios.defaults.baseURL = 'http://localhost:8080/CloudService/rest';

const Navigation = { template: '<navigation></navigation>' }
const Login = { template: '<log-in></log-in>' }
const OrganizacijePregled = { template: '<organizacije></organizacije>' }
const DodajIzmeniOrganizaciju = { template: '<dodaj-izmeni-organizaciju></dodaj-izmeni-organizaciju>' }
const KorisniciPregled = { template: '<korisnici></korisnici>' }
const DodajIzmeniKorisnika = { template: '<dodaj-izmeni-korisnika></dodaj-izmeni-korisnika>' }
const MojProfil = { template: '<moj-profil></moj-profil>' }
const VirtualneMasinePregled = { template: '<virtualne-masine></virtualne-masine>' }
const DodajIzmeniVirtualnuMasinu = { template: '<dodaj-izmeni-virtualnu-masinu></dodaj-izmeni-virtualnu-masinu>' }
const DiskoviPregled = { template: '<diskovi></diskovi>' }
const DodajIzmeniDisk = { template: '<dodaj-izmeni-disk></dodaj-izmeni-disk>' }

const router = new VueRouter({
	
	mode: 'hash',
	routes: [
		{
			path: '/',
			components: {
				default: Navigation,
				div: Login
			},
			meta: { roles: ["korisnik", "admin", "superAdmin", null, "null"] }
		},
		{
			path: '/cloudy/login',
			components: {
				default: Navigation,
				div: Login
			},
			meta: { roles: ["korisnik", "admin", "superAdmin", null, "null"] }
		},
		{
			path: '/organizacije/pregled',
			components: {
				default: Navigation,
				div: OrganizacijePregled
			},
			meta: { roles: ["superAdmin"] }
			
		},
		{
			path: '/organizacije/mod',
			components: {
				default: Navigation,
				div: DodajIzmeniOrganizaciju
			},
			meta: { roles: ["admin", "superAdmin"] }
		},
		{
			path: '/korisnici/pregled',
			components: {
				default: Navigation,
				div: KorisniciPregled
			},
			meta: { roles: ["admin", "superAdmin"] }
		},
		{
			path: '/korisnici/mod',
			components: {
				default: Navigation,
				div: DodajIzmeniKorisnika

			},
			meta: { roles: ["admin", "superAdmin"] }
		},
		{
			path: '/cloudy/mojProfil',
			components: {
				default: Navigation,
				div: MojProfil

			},
			meta: { roles: ["korisnik", "admin", "superAdmin"] }
		},		
		{
			path: '/virtualneMasine/pregled',
			components: {
				default: Navigation,
				div: VirtualneMasinePregled

			},
			meta: { roles: ["korisnik", "admin", "superAdmin"] }
		},
		{
			path: '/virtualneMasine/mod',
			components: {
				default: Navigation,
				div: DodajIzmeniVirtualnuMasinu

			},
			meta: { roles: ["korisnik", "admin", "superAdmin"] }
		},
		{
			path: '/diskovi/pregled',
			components: {
				default: Navigation,
				div: DiskoviPregled

			},
			meta: { roles: ["korisnik", "admin", "superAdmin"] }
		},
		{
			path: '/diskovi/mod',
			components: {
				default: Navigation,
				div: DodajIzmeniDisk

			},
			meta: { roles: ["korisnik", "admin", "superAdmin"] }
		}
	]

});

router.beforeEach((to, from, next) => {
	console.log(sessionStorage.getItem("tip"));
    if (to.meta.roles.includes(sessionStorage.getItem("tip"))) {
		next();
	  }
	  else{
		  alert("Forbidden");
	  }
    
});

var app = new Vue({
	router,
	
	el: '#divWelcome',

	mounted: function () {
		//alert(sessionStorage.getItem("tip"));

	},
	updated: function () {
		//alert(sessionStorage.getItem("tip"));

	}

});