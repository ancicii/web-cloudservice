Vue.component("diskovi", {
	data: function () {
		return {
			diskovi: null,
			uloga: sessionStorage.getItem("tip")
		}
	},
	template: `
	<div class="organizacije">
    <div class="container organiz text-dark bg-white mt-5 pb-2 mb-3">
      <div class="row p-4 bg-mobile">
        <div class="col-md-6">
          <h2>Diskovi</h2>
        </div>
        <div class="col-md-6 ">
          <button @click="funct('ne', null)" v-if="uloga!='korisnik'" class="nav-link btn btn-custom pull-right">Dodaj</button>
        </div>
			</div>
			<div class="row p-3 effects" >
		  		<div class="col-md-4" style="min-height:20px;">
						<h3 style="color:#824E71;">IME</h3>
				  </div>
				<div class="col-md-4" style="min-height:20px;">
				<h3 style="color:#824E71;">KAPACITET</h3>
		  		</div>
		  		<div class="col-md-4" style="min-height:20px;">
        <h3 style="color:#824E71;">VIRTUALNA MASINA</h3>
      </div>
		</div>
		<hr>
				<div v-for="disk in diskovi" class="row p-3 effects" >
		  		<div class="col-md-4" style="min-height:90px;">
					 <p class="text-info text-center">{{disk.ime}}</p>
				</div>
				<div class="col-md-4" style="min-height:90px;">
					 <p class="text-info text-center">{{disk.kapacitet}}</p>
		  		</div>
		  		<div class="col-md-4" style="min-height:90px;">
					 <p class="text-info text-center">{{disk.virtualnaMasina}}</p>
      </div>
	  </div>
	  </div>
	</div>
	`,
	methods: {
		funct: function (w, event) {
			imeDiska = null;

			if (event != null) {
				parent = event.target.parentElement;
				parentClass = event.target.parentElement.className;
				if (parentClass === "row p-3 effects") {
					imeDiska = parent.childNodes[0].innerText;
				}
				else {
					parent = parent.parentElement;
					imeDiska = parent.childNodes[0].innerText;
				}
			}
			console.log(imeDiska);
			sessionStorage.setItem("izmeniDisk", w);
			sessionStorage.setItem("imeDiska", imeDiska);
			router.push('/diskovi/mod');
		}
	},
	mounted() {
		axios.
			get('/diskovi/pregled', { headers: { Authorization: sessionStorage.getItem("Authorization") } })
			.then(response => {
				this.diskovi = response.data;		
			});
	}


})