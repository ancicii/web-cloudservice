package dto;

import java.util.Date;

import beans.Aktivnost;

public class AktivnostDTO {

	private Date datumUpaljena;
	private Date datumUgasena;
	
	public Date getDatumUpaljena() {
		return datumUpaljena;
	}
	public void setDatumUpaljena(Date datumUpaljena) {
		this.datumUpaljena = datumUpaljena;
	}
	public Date getDatumUgasena() {
		return datumUgasena;
	}
	public void setDatumUgasena(Date datumUgasena) {
		this.datumUgasena = datumUgasena;
	}
	public AktivnostDTO(Date datumUpaljena, Date datumUgasena) {
		super();
		this.datumUpaljena = datumUpaljena;
		this.datumUgasena = datumUgasena;
	}
	public AktivnostDTO() {
		super();
	}
	public AktivnostDTO(Aktivnost aktivnost) {
		super();
		if(aktivnost.getDatumUgasena() == null) {
			this.datumUgasena=null;
		}else {
			this.datumUgasena = Date.from(aktivnost.getDatumUgasena().toInstant());
		}if(aktivnost.getDatumUpaljena() == null) {
			this.datumUpaljena = null;
		}else {
			this.datumUpaljena = Date.from(aktivnost.getDatumUpaljena().toInstant());
		}
		
		
		
	}
	
}
