package dto;


public class PregledDiskovaDTO {
	private String ime;
	private int kapacitet;
	private String virtualnaMasina;
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public int getKapacitet() {
		return kapacitet;
	}
	public void setKapacitet(int kapacitet) {
		this.kapacitet = kapacitet;
	}
	public String getVirtualnaMasina() {
		return virtualnaMasina;
	}
	public void setVirtualnaMasina(String virtualnaMasina) {
		this.virtualnaMasina = virtualnaMasina;
	}
	public PregledDiskovaDTO(String ime, int kapacitet, String virtualnaMasina) {
		super();
		this.ime = ime;
		this.kapacitet = kapacitet;
		this.virtualnaMasina = virtualnaMasina;
	}
	public PregledDiskovaDTO() {
		super();
	}
	
	
}
