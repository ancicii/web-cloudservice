package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import beans.Disk;
import beans.Korisnik;
import beans.Organizacija;
import beans.TipDiska;
import beans.VirtualnaMasina;
import dto.DodajDiskDTO;
import dto.PregledDiskovaDTO;

@DeclareRoles({ "SUPER_ADMIN", "ADMIN", "KORISNIK" })
@Path("/diskovi")
public class ServiceDiskovi {
	
	CloudService cs = new CloudService();
	
	@Context
    SecurityContext securityContext;
	
	@GET
	@RolesAllowed({"SUPER_ADMIN", "ADMIN"})
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSlobodniDiskovi() {
		Collection<Disk> list = new ArrayList<Disk>();
		
		for(Disk d: cs.diskovi.getDiskovi().values()) {
			if(d.getVirtualnaMasina()==null) {
				list.add(d);
			}
		}
		return Response.status(Response.Status.OK).entity(list).build();
	}
	
	@GET
	@RolesAllowed({ "SUPER_ADMIN", "ADMIN", "KORISNIK" })
	@Path("/pregled")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getVirtualneMasine() {
		Collection<PregledDiskovaDTO> list = new ArrayList<PregledDiskovaDTO>();
		Korisnik korisnik = (Korisnik) securityContext.getUserPrincipal();

		if (korisnik.getUloga().toString().equalsIgnoreCase("SUPER_ADMIN")) {
			for(Disk disk: cs.diskovi.getDiskovi().values()) {
				if(disk.getVirtualnaMasina()==null) {
					list.add(new PregledDiskovaDTO(disk.getIme(), disk.getKapacitet(), "nije zakacen"));
				}else {
					list.add(new PregledDiskovaDTO(disk.getIme(), disk.getKapacitet(), disk.getVirtualnaMasina().getIme()));
				}
				
			}
		} else {
			for (Organizacija o : cs.organizacije.getOrganizacije().values()) {
				if (cs.korisnici.getKorisnici().get(korisnik.getEmail()).getOrganizacija().getIme()
						.equals(o.getIme())) {
					for (Object resurs : o.getResursi()) {
						if (resurs instanceof Disk) {
							Disk disk = (Disk) resurs;
							list.add(new PregledDiskovaDTO(disk.getIme(), disk.getKapacitet(), disk.getVirtualnaMasina().getIme()));
						}
					}
				}
			}
		}
		return Response.status(Response.Status.OK).entity(list).build();
	}
	
	@POST
	@RolesAllowed({"SUPER_ADMIN", "ADMIN"})
	@Path("/dodaj")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response dodajDisk(DodajDiskDTO diskDTO) {
		VirtualnaMasina vm = null;
		if (diskDTO.getIme().length() == 0 || diskDTO.getTip().length() == 0) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Morate popuniti sva polja").build();
		} else if (diskDTO.getIme() == null || diskDTO.getTip() == null || diskDTO.getKapacitet() == 0) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Morate popuniti sva polja").build();
		}if(diskDTO.getKapacitet()<=0) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Kapacitet diska mora biti pozitivna vrednost!").build();
		}
		if (cs.diskovi.getDiskovi().containsKey(diskDTO.getIme())) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Vec postoji disk sa unetim imenom").build();
		}
		if(diskDTO.getVirtualnaMasina().length()!=0) {
			vm = cs.virtualneMasine.getVirtualneMasine().get(diskDTO.getVirtualnaMasina());			
		}
		Disk disk = new Disk(diskDTO.getIme(), TipDiska.valueOf(diskDTO.getTip()), diskDTO.getKapacitet(), vm);
		cs.diskovi.getDiskovi().put(disk.getIme(), disk);
		cs.upisiDiskove();
		if(diskDTO.getVirtualnaMasina().length()!=0) {
			vm.getDiskovi().add(disk);
			Organizacija o = null;
			for(Organizacija org: cs.organizacije.getOrganizacije().values()) {
				for(Object resurs: org.getResursi()) {
					if(resurs instanceof VirtualnaMasina) {
						VirtualnaMasina virtualna = (VirtualnaMasina) resurs;
						if(virtualna.getIme().equals(vm.getIme())){
							org.getResursi().add(disk);						
							break;
						}
					}
				}
			}
			cs.upisiOrganizacije();
			cs.upisiVirtualneMasine();
		}
		
		return Response.status(Response.Status.OK).entity("Uspesno dodat disk!").build();

	}
	
	
	
	
}
