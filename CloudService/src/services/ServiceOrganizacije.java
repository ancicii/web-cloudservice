package services;

import java.util.Collection;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Organizacija;
import dto.OrganizacijaDTO;

@DeclareRoles({
    "SUPER_ADMIN",
    "ADMIN",
    "KORISNIK"
})
@Path("/organizacije")
public class ServiceOrganizacije {
	
	CloudService cs = new CloudService();
	
	@RolesAllowed("SUPER_ADMIN")
	@GET
	@Path("/pregled")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Organizacija> getOrganizacije() {
		Collection<Organizacija> list = cs.organizacije.getOrganizacije().values();
		return list;
	}

	@RolesAllowed("SUPER_ADMIN")
	@POST
	@Path("/dodaj")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response dodajOrganizaciju(OrganizacijaDTO organizacijaDTO) {
		if (organizacijaDTO.getIme().length() == 0 || organizacijaDTO.getOpis().length() == 0) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Morate popuniti sva polja").build();
		} else if (organizacijaDTO.getIme() == null || organizacijaDTO.getOpis() == null) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Morate popuniti sva polja").build();
		}
		if (cs.organizacije.getOrganizacije().containsKey(organizacijaDTO.getIme())) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Vec postoji organizacija sa unetim imenom")
					.build();
		}
		Organizacija novaOrganizacija = organizacijaDTO.toOrganizacija();
		cs.organizacije.getOrganizacije().put(novaOrganizacija.getIme(), novaOrganizacija);
		cs.upisiOrganizacije();
		return Response.status(Response.Status.OK).entity("Uspesno dodata organizacija").build();

	}
	
	@PUT
	@RolesAllowed({"SUPER_ADMIN", "ADMIN"})
	@Path("/izmeni/{naziv}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response izmeniOrganizaciju(OrganizacijaDTO organizacijaDTO, @PathParam("naziv") String naziv){
		if (organizacijaDTO.getIme().length() == 0 || organizacijaDTO.getOpis().length() == 0) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Morate popuniti sva polja").build();
		} else if (organizacijaDTO.getIme() == null || organizacijaDTO.getOpis() == null) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Morate popuniti sva polja").build();
		}
		if (cs.organizacije.getOrganizacije().containsKey(organizacijaDTO.getIme())&& (!organizacijaDTO.getIme().equalsIgnoreCase(naziv))) {
			return Response.status(Response.Status.BAD_REQUEST).entity("Vec postoji organizacija sa unetim imenom").build();
		}
		Organizacija staraOrganizacija = cs.organizacije.getOrganizacije().get(naziv);
		staraOrganizacija.setIme(organizacijaDTO.getIme());
		staraOrganizacija.setLogo(organizacijaDTO.getLogo());
		staraOrganizacija.setOpis(organizacijaDTO.getOpis());
		cs.organizacije.getOrganizacije().remove(naziv);
		cs.organizacije.getOrganizacije().put(staraOrganizacija.getIme(), staraOrganizacija);
		cs.upisiOrganizacije();
		return Response.status(Response.Status.OK).entity("Uspesno izmenjena organizacija").build();
	}
	
	@GET
	@RolesAllowed({"SUPER_ADMIN", "ADMIN"})
	@Path("/get/{naziv}")
	@Produces(MediaType.APPLICATION_JSON)
	public Organizacija getOrganizacija(@PathParam("naziv") String naziv) {
		Organizacija organizacija = null;
		if(cs.organizacije.getOrganizacije().containsKey(naziv)) {
			organizacija = cs.organizacije.getOrganizacije().get(naziv);
			return organizacija;
		}else {
			return null;
		}
	}
		
}
