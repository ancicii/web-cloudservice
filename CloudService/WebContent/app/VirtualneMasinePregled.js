
Vue.component("virtualne-masine", {
    data: function () {
        return {
            virtualneMasine: null,
            uloga: "", pretraga: {
                pocetnaD: "",
                krajnjaD: "",
                drzava: "",
                datum: ""
            },
            params: {
                jezgraOd: "",
                jezgraDo: "",
                ramOd: "",
                ramDo: "",
                gpuOd: "",
                gpuDo: "",
                nazivVM: ""
            }
        }
    },
    template: `
<div class="organizacije">
    <div class="row search">
        <div class="form-group col">
            <label >
            <p class="font" style="color: rgb(130, 78, 113);font-weight: 700;">Broj jezgara</p></label>
            <div class="row" style="margin-top:-20px">
                <div class="col">
                <input type="number" class="form-control" id="jezgraOd" name="brojJezgaraRangeOd" min="0" placeholder="od" v-model="params.jezgraOd">                
                </div>
                <div class="col">
                <input type="number" class="form-control" style="margin-left:-20px" id="jezgraDo" name="brojJezgaraRangeDo" min="0" placeholder="do" v-model="params.jezgraDo">
                </div>
            </div>
        </div>
        <div class="form-group col">
           <label >
            <p class="font" style="color: rgb(130, 78, 113);font-weight: 700;">RAM</p></label>
            <div class="row" style="margin-top:-20px">
                <div class="col">
                <input type="number" class="form-control" id="ramOd" name="ramOd" min="0" placeholder="od" v-model="params.ramOd">                
                </div>
                <div class="col">
                <input type="number" class="form-control" style="margin-left:-20px" id="ramDo" name="ramDo" min="0" placeholder="do" v-model="params.ramDo">
                </div>
            </div>
        </div>
        <div class="form-group col">
           <label >
            <p class="font" style="color: rgb(130, 78, 113);font-weight: 700;">GPU jezgra</p></label>
            <div class="row" style="margin-top:-20px">
                <div class="col">
                <input type="number" class="form-control" id="gpuOd" name="gpuOd" min="0" placeholder="od" v-model="params.gpuOd">                
                </div>
                <div class="col">
                <input type="number" class="form-control" style="margin-left:-20px" id="gpuDo" name="gpuDo" min="0" placeholder="do" v-model="params.gpuDo">
                </div>
            </div>
        </div>       
    </div>

    <div class="row">
    <div class="form-group col">
           <label >
            <p class="font" style="color: rgb(130, 78, 113);font-weight: 700;">Naziv</p></label>
            <div class="row" style="margin-top:-20px">
                <div class="col">
                <input type="text" class="form-control" id="nazivVM" name="nazivVM" placeholder="Pretrazite virtualne masine po nazivu" v-model="params.nazivVM">
                </div>
            </div>
        </div>
        
    </div>
    <div class="row">
        <div class="col" style="width:100%;height:30%;text-align:right;position:relative;top:40%;">
            <button @click = "search" class="nav-link btn float-right"><i class="fa fa-search fa-lg" aria-hidden="true"></i></button>
        </div>
        <div class="col" style="width:100%;height:30%;text-align:left;position:relative;top:40%">
            <button @click = "ponisti" class="nav-link btn"><i class="fa fa-times fa-2x" aria-hidden="true"></i></button>
        </div>
    </div>

    <div class="container organiz text-dark bg-white mt-5 pb-2 mb-3">
        <div class="row p-4 bg-mobile">
            <div class="col-md-6">
                <h2>Virtualne Masine</h2>
            </div>
            <div class="col-md-6 ">
                <button @click="funct('ne', null)" v-if="uloga!=='korisnik'" class="nav-link btn btn-custom pull-right">Dodaj</button>
            </div>
        </div>
        <div class="row p-3 effects">
            <div class="col-md-3" v-if="uloga === 'superAdmin'" style="min-height:20px;">
                <h3 style="color:#824E71;">IME</h3>
			</div>  
			<div class="col-md-3" v-if="uloga !== 'superAdmin'" style="min-height:20px;">
                <h3 style="color:#824E71;">IME</h3>
            </div>            
            <div class="col-md-3" v-if="uloga === 'superAdmin'" style="min-height:20px;">
                <h3 style="color:#824E71;">BROJ JEZGARA</h3>
			</div>
			<div class="col-md-4" v-if="uloga !== 'superAdmin'" style="min-height:20px;">
                <h3 style="color:#824E71;">BROJ JEZGARA</h3>
            </div>
            <div class="col-md-1" v-if="uloga === 'superAdmin'" style="min-height:20px;">
                <h3 style="color:#824E71;">RAM</h3>
			</div>
			<div class="col-md-2" v-if="uloga !== 'superAdmin'" style="min-height:20px;">
                <h3 style="color:#824E71;">RAM</h3>
            </div>
            <div class="col-md-2" v-if="uloga === 'superAdmin'" style="min-height:20px;">
                <h3 style="color:#824E71;">GPU</h3>
			</div>
			<div class="col-md-2" v-if="uloga !== 'superAdmin'" style="min-height:20px;">
                <h3 style="color:#824E71;">GPU</h3>
            </div>
            <div class="col-md-3" v-if="uloga === 'superAdmin'" style="min-height:20px;">
                <h3 style="color:#824E71;">ORGANIZACIJA</h3>
            </div>
        </div>
        <hr>
        <div v-for="vm in virtualneMasine" class="row p-3 effects"  @click="funct('da',$event)">
            <div class="col-md-3" v-if="uloga !== 'superAdmin'" style="min-height:90px;">
                <p class="text-info text-center">{{vm.ime}}</p>
            </div>
            <div class="col-md-3" v-if="uloga === 'superAdmin'" style="min-height:90px;">
                <p class="text-info text-center">{{vm.ime}}</p>
            </div>
            <div class="col-md-3" v-if="uloga === 'superAdmin'" style="min-height:90px;">
                <p class="text-info text-center">{{vm.kategorija.brojJezgara}}</p>
            </div>
            <div class="col-md-4" v-if="uloga !== 'superAdmin'" style="min-height:90px;">
                <p class="text-info text-center">{{vm.kategorija.brojJezgara}}</p>
            </div>
            <div class="col-md-2" v-if="uloga !== 'superAdmin'" style="min-height:90px;">
                <p class="text-info text-center">{{vm.kategorija.ram}}</p>
            </div>
            <div class="col-md-1" v-if="uloga === 'superAdmin'" style="min-height:90px;">
                <p class="text-info text-center">{{vm.kategorija.ram}}</p>
			</div>    
			<div class="col-md-2" v-if="uloga !== 'superAdmin'" style="min-height:90px;">
                <p class="text-info text-center">{{vm.kategorija.gpujezgra}}</p>
            </div>
            <div class="col-md-2" v-if="uloga === 'superAdmin'" style="min-height:90px;">
                <p class="text-info text-center">{{vm.kategorija.gpujezgra}}</p>
            </div>        
            <div class="col-md-3" v-if="uloga === 'superAdmin'" style="min-height:90px;">
                <p class="text-info text-center">{{vm.organizacija.ime}}</p>
            </div>            
        </div>
    </div>
</div>
	`,
    methods: {        
        funct: function (w, event) {
            imeVM = null;

            if (event != null) {
                parent = event.target.parentElement;
                parentClass = event.target.parentElement.className;
                if (parentClass === "row p-3 effects") {
                    if (this.uloga !== "superAdmin") {
                        imeVM = parent.childNodes[0].innerText;
                    } else {
                        imeVM = parent.childNodes[2].innerText;
                    }

                } else {
                    parent = parent.parentElement;
                    if (this.uloga !== "superAdmin") {
                        imeVM = parent.childNodes[0].innerText;
                    } else {
                        imeVM = parent.childNodes[2].innerText;
                    }
                }
            }
            sessionStorage.setItem("izmeniVM", w);
            sessionStorage.setItem("imeVM", imeVM);

            router.push('/virtualneMasine/mod');


        },
        search: function(){
            let params = {
                jezgraOd: this.params.jezgraOd,
                jezgraDo: this.params.jezgraDo,
                ramOd: this.params.ramOd,
                ramDo: this.params.ramDo,
                gpuOd: this.params.gpuOd,
                gpuDo: this.params.gpuDo,
                nazivVM: this.params.nazivVM
            };
            axios.
            put('/virtualneMasine/pretrazi', params, {
                headers: {Authorization: sessionStorage.getItem("Authorization")}})
            .then(response => {
                console.log(response.data);
                this.virtualneMasine = response.data;
                //go();

            });
        },
        ponisti: function(){
           axios.
            get('/virtualneMasine/pregled', {
                headers: {
                    Authorization: sessionStorage.getItem("Authorization")
                }
            })
			.then(response => {
				this.virtualneMasine = response.data;
			});
			this.params.jezgraOd="";
			this.params.jezgraDo="";
			this.params.ramOd="";
			this.params.ramDo="";
			this.params.gpuOd="";
			this.params.gpuDo="";
			this.params.nazivVM="";   
        }
    },
    mounted() {        
        this.uloga = sessionStorage.getItem("tip");
        axios.
            get('/virtualneMasine/pregled', {
                headers: {
                    Authorization: sessionStorage.getItem("Authorization")
                }
            })
            .then(response => {
                this.virtualneMasine = response.data;

            });
        
    }
})
