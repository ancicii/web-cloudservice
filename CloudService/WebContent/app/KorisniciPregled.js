Vue.component("korisnici", {
    data: function() {
        return {
            korisnici: null,
            uloga: ""
        }
    },
    template: `
<div class="organizacije">
    <div class="container organiz text-dark bg-white mt-5 pb-2 mb-3">
        <div class="row p-4 bg-mobile">
            <div class="col-md-6">
                <h2>Korisnici</h2>
            </div>
            <div class="col-md-6 ">
                <button @click="funct('ne', null)" class="nav-link btn btn-custom pull-right">Dodaj</button>
            </div>
        </div>
        <div class="row p-3 effects">
            <div class="col-md-4" v-if="uloga === 'admin'" style="min-height:20px;">
                <h3 style="color:#824E71;">EMAIL</h3>
            </div>
            <div class="col-md-3" v-if="uloga === 'superAdmin'" style="min-height:20px;">
                <h3 style="color:#824E71;">EMAIL</h3>
            </div>
            <div class="col-md-3" v-if="uloga === 'superAdmin'" style="min-height:20px;">
                <h3 style="color:#824E71;">IME</h3>
            </div>
            <div class="col-md-4" v-if="uloga === 'admin'" style="min-height:20px;">
                <h3 style="color:#824E71;">IME</h3>
            </div>
            <div class="col-md-4" v-if="uloga === 'admin'" style="min-height:20px;">
                <h3 style="color:#824E71;">PREZIME</h3>
            </div>
            <div class="col-md-3" v-if="uloga === 'superAdmin'" style="min-height:20px;">
                <h3 style="color:#824E71;">PREZIME</h3>
            </div>
            <div class="col-md-3" v-if="uloga === 'superAdmin'" style="min-height:20px;">
                <h3 style="color:#824E71;">ORGANIZACIJA</h3>
            </div>
        </div>
        <hr>
        <div v-for="kor in korisnici" class="row p-3 effects"  @click="funct('da',$event)">
            <div class="col-md-4" v-if="uloga === 'admin'" style="min-height:90px;">
                <p class="text-info text-center">{{kor.email}}</p>
            </div>
            <div class="col-md-3" v-if="uloga === 'superAdmin'" style="min-height:90px;">
                <p class="text-info text-center">{{kor.email}}</p>
            </div>
            <div class="col-md-3" v-if="uloga === 'superAdmin'" style="min-height:90px;">
                <p class="text-info text-center">{{kor.ime}}</p>
            </div>
            <div class="col-md-4" v-if="uloga === 'admin'" style="min-height:90px;">
                <p class="text-info text-center">{{kor.ime}}</p>
            </div>
            <div class="col-md-4" v-if="uloga === 'admin'" style="min-height:90px;">
                <p class="text-info text-center">{{kor.prezime}}</p>
            </div>
            <div class="col-md-3" v-if="uloga === 'superAdmin'" style="min-height:90px;">
                <p class="text-info text-center">{{kor.prezime}}</p>
            </div>            
            <div class="col-md-3" v-if="uloga === 'superAdmin'" style="min-height:90px;">
                <p class="text-info text-center">{{kor.organizacija.ime}}</p>
            </div>            
        </div>
    </div>
</div>
	`,
    methods: {
        funct: function(w, event) {
            email = null;

            if (event != null) {
                parent = event.target.parentElement;
                parentClass = event.target.parentElement.className;
                if(parentClass==="row p-3 effects"){
                	if(this.uloga==="admin"){
                		email = parent.childNodes[0].innerText;
                	}else{
                		email = parent.childNodes[2].innerText;
                	}
                	
                }else{
                	parent = parent.parentElement;
                	if(this.uloga==="admin"){
                		email = parent.childNodes[0].innerText;
                	}else{
                		email = parent.childNodes[2].innerText;
                	}
                }
            }
            sessionStorage.setItem("izmeniK", w);
            sessionStorage.setItem("emailKorisnika", email);
            if(email === sessionStorage.getItem("korisnik")){
            	router.push('/cloudy/mojProfil');
            }else{
            	router.push('/korisnici/mod');
            }
            
        }
    },
    mounted() {
        this.uloga = sessionStorage.getItem("tip");
        axios.
        get('/korisnici/pregled', {
                headers: {
                    Authorization: sessionStorage.getItem("Authorization")
                }
            })
            .then(response => {
                this.korisnici = response.data;

            });
    }
})
